## Objectif

Cette procédure permet d'installer docker.

## Prérequis

* OS Debian installé

## Installation, configuration de Docker

* Passer en mode root

```bash
su -
```

* Mettre à jour le cache apt

```bash
apt update
```

* Installer quelques paquets nécessaire

```bash
apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```

* Installer la clé et l'emplacement du dépôt Docker

```bash
curl –fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt update
```

* Installer Docker

```bash
apt install docker-ce
docker version
```

* Mettre votre utilisateur dans le groupe 'docker' *(Falcutatif)*

```bash
usermod -aG docker Votre_compte_utilisateur
exit
exit
```

* Reconnecter vous à la machine en SSH

### Installation, configuration de Docker-compose

```bash
su -
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod 755 /usr/local/bin/docker-compose
```
