## Objectif

Cette procédure permet d'installer un OS debian

## Prérequis

* Accés au vCenter de la plateforme Recherche & Développement

## Configuration minimale

| Machine   | Minimum CPU | Minimum RAM |
| ------------ |:-------------:| :-------------:|
| xxxxx       | 1vCPU         | 1 Go|

## Installation

* Installer l'OS Debian

### Installation de l'OS Debian

* Installer votre debian serveur SANS de mode X ou autres.
* Installer openssh pour avoir le serveur SSH actif.
* Logguer vous une fois terminé.

### Fin d'installation

* Passer en mode root

```bash
su -
```

* Supprimer nano (pour les aficionados de vi :) )

```bash
apt purge nano
```

* Mettre à jour le cache apt

```bash
apt update
```
