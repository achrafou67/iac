# Installation d'un montage NFS

| **SPIE ICS – Direction des Activités Spécialisées** |   |   |
| -- | :--: | -- |
| ISO27K-SMSI-Installation d'un montage NFS | RESTREINT | 12/03/2020 |

| ***Etat du document*** |                           |                         |                        |                              |
| ---------------- | ------------------------- | ----------------------- | ---------------------- | ---------------------------- |
| Provisoire       :white_large_square: | Pour Validation :white_large_square: |  **Définitif** :white_check_mark: | Périmé :white_large_square: | A Supprimer :white_large_square: |

| ***Circuit de validation*** | Nom Prénom | Fonction | Date | Signature |
| --------------------------: | ---------- | -------- | :--: | :-------: |
| Rédaction | Sébastien DENICHOUX | Responsable Développement | 12/03/2020 | :white_check_mark: |
| Relecteur | Emmanuel ANDRE | Responsable Infrastructure | 17/03/2020 | :white_check_mark: |
| Relecteur | Bogdan SZANTO | Responsable Centre de Compétences | 31/08/2020 | :white_check_mark: |
| Validation | Mathieu BICHON | RSSI | 07/10/2020 | :white_check_mark: |

## Objectif

Cette procédure permet d'installer et configurer un montage NFS.

## Prérequis

* OS Debian installé

## Installation, configuration

* Passer en mode root

```bash
su -
```

* Installer le paquet nfs-common

```bash
apt install nfs-common
```

* Créer le point de montage

```bash
cd /mnt
mkdir nas-depot
```

* Tester le point de montage

```bash
mount -t nfs XXX.XXX.XXX.XXX:/yyyyyy/DEPOT -O user="XXXXXXX",pass="XXXXXXX" /mnt/nas-depot/
df -h
```

* Configurer le montage automatique au démarrage

```bash
cd /etc/network/if-up.d
touch 99-mount-nas
chmod 755 99-mount-nas
vi 99-mount-nas
```

* Copier ce code

```bash
#!/bin/bash
mount -t nfs XXX.XXX.XXX.XXX:/yyyyyy/DEPOT -O user="XXXXX",pass="XXXXX" /mnt/nas-depot/
```

Ne pas oublier de changer les XXXXX et yyyyy :).

* Redémarrer

```bash
reboot
```

* Se logger

* Passer en root

```bash
su -
```

* Tester la présence du montage

```bash
df -h
```
