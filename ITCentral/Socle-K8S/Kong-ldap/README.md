## Objectifs

- Mettre en place une API d'authentification LDAP
- Utilisation de cette API pour obtenir un Token sécurisé
- Création d'un "customer" Kong pour l'utilisateur authentifié avec les infos pour valider le Token
- Utilisation du plugin JWT sur un service pour authentifier un customer

## Prérequis

- Kong Proxy, Kong Admin API sont installé sur le cluster K8S.
- Avoir un LDAP fonctionnel (Active Directory), en LDAPS

## Installation

- Initialiser une connexion docker login vers le dépot registry

- Sur le master kubernetes, récupérer le fichier dans le répertoire deploy

- Modifier les variables d'environnement en fonction de votre infra  
  [Variables](#variables-denvironnements-du-déploiement)

- Une fois les variables d'environnement définie lancer le déploiement:

```bash
kubectl apply -f kongldap.yml
```

- Vérifier que le pod est bien `running`

```bash
kubectl get pods -n kong
```

## Utilisation

Dans la configuraiton du déploiement, le service crée pour accéder au pod à été mis en "NodePort" (valeur par défaut 31080).

Pour appeler l'API il suffit donc de lancer les commandes suivantes:

```bash
curl -kv https://adresse_master:nodeport/login/ \
--header 'Authorization: Basic dXNlcm5hbWU6cGFzc3dvcmQ='
```

Ce qui nous donne (pour un vrai entête avec des crédentials Ldap du type sAMAccountName:password) :

```yaml
```

> Note:`dXNlcm5hbWU6cGFzc3dvcmQ=` a été obtenu en appliquant la fonction base64 (`https://www.base64encode.org/`) en passant la chaine "username:password"

## Processus de fonctionnement de l'API Endpoint LDAP

- Recéption d'une requête GET sur l'endpoint `/login` avec un entête: `Authorization: Basic base64(username:password)`
- Vérification du couple username/password sur LDAP ou retour d'une erreur `401`
- Appel de l'API KongCall Kong admin API et si l'utilisateur kong `ldap_USERNAME`n'existe pas alors le créer
- Récupération des groupes LDAP de l'utilisateur
- Pour chaque groupes récupérés dans LDAP depuis la base `LOGINEP_GROUP_BASE_dn` vérifier si l'utilisateur en fait partie dans LDAP.
- Suppression des ACLs de l'utilisateur Kong, qu'il ne doit pas avoir
- Ajout des ACLs  (groupes) de l'utilisateur Kong, qu'il doit avoir
- Ajout des informations JWT à l'utilisateur kong
- Génération d'un Token JWT à partir des infos et ajout d'une date d'expiration en fonction de la variable d'enbironnement LOGINEP_JWT_TOKEN_TIMEOUT
- Réponse à l'appelant de l'API avec le Token

## Variables d'environnements du déploiement

 | Variable d'environnement | Exemple de valeur | Description |
 | ------------------------ | ------------- | ------- |
 | LOGINEP_LDAP_TIMEOUT | 60 | Timeout pour la connexion LDAP |
 | LOGINEP_LDAP_HOST | 192.168.111.231 | Adresse IP ou DNS du serveur LDAP |
 | LOGINEP_LDAP_PORT | 389 ou 636 (SSL) | Port du serveur LDAP |
 | LOGINEP_LDAP_CONSUMERCLIENTID_PREFIX | ldap_ | Prefixe à ajouter sur l'utilisateur ldap crée dans KONG |
 | LOGINEP_USER_BASE_DN | dc=ADM,dc=INFRA | La base du DN pour les requêtes LDAP |
 | LOGINEP_USER_OBJ_CLASS | user | Classe de l'objet désiré dans LDAP pour rechercher l'utilisateur |
 | LOGINEP_USER_ATTRIBUTE | sAMAccountName | Attribut de l'utilisateur LDAP pour la connexion |
 | LOGINEP_GROUP_BASE_DN | ou=ITCentral,ou=Applications,dc=ADM,dc=INFRA | La base DN pour rechercher les groupes  |
 | LOGINEP_GROUP_OBJ_CLASS | group | Classe de l'objet désiré dans LDAP pour rechercher le group |
 | LOGINEP_ROLES | user, manager, ..., admin | Liste des différents rôles possibles (qui seront récupérer dans les groupes dans l'OU) |
 | LOGINEP_KONG_ADMINAPI_URL | https://kong:8001 | Adresse du serveur Kong Admin |
 | LOGINEP_KONG_API_KEY | - | API Key utilisé pour sécurisé le serveur Kong Admin (optionnel) | 
 | LOGINEP_JWT_TOKEN_TIMEOUT | 10800 | Période de validité pour un Token JWT produit. Une fois expiré les utilisateurs doivent en récupérer un nouveau. |
