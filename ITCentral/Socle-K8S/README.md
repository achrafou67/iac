## Objectif

Ce guide a pour objectif de donner les bases pour l'installation et la mise en place des composants essentiels pour une archiecture Microservices sous K8S.

Les différentes étapes à réaliser sont décrites dans l'odre dans le sommaire suivant.  

## Sommaire

1. [Déploiement d'un cluster K8S avec ansible](./ITCentral/Socle-K8S/ansible/README.md)
2. [Mise en place d'un provisionner de volume](./ITCentral/Socle-K8S/nfs/README.md)
3. [Mise en place de Ingress Contoller Kong, Kong Proxy et HAproxy](./ITCentral/Socle-K8S/kong-ingress-controller/README.md)
4. [Tutorial sur la génération de certificat k8s et utilisation dans un ingress](./ITCentral/Socle-K8S/kubernetes/README.md)
5. [Mise en place de Konga](./ITCentral/Socle-K8S/konga/README.md)
6. [Mise en place de l'API GW Kong et raccordement à Konga](./ITCentral/Socle-K8S/kong-apigateway/README.md)
7. [Mise en place d'un endpoint d'authentification LDAP raccordé à Kong](./ITCentral/Socle-K8S/kong-ldap-jwt/README.md)
8. [Mise en place de RabbitMQ avec raccordement LDAP](./ITCentral/Socle-K8S/rabbitmq/README.md)
