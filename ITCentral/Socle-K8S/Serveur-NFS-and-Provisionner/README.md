## Objectif

Monter un serveur NFS sur une machine dédiée avec un point de montage visible par le cluster k8s.

## Prérequis

* 1 machine Debian (>=9)

| Machine   | Minimum CPU | Minimum RAM |
| ------------- |:-------------:| :-------------:|
| nfs       | 1       |2 Go|

* Configuration du SSH

## Montage du serveur NFS

### Dépendances

Installer les paquets apt permettant de deployer un serveur nfs

```bash
sudo apt-get update
sudo apt install nfs-kernel-server nfs-server
```

### Configurer le serveur NFS

* Créer un dossier à partager

```bash
sudo mkdir -p /mnt/storage/dynamic
```

* Modifier le fichier `/etc/exports` pour exporter le dossier à partager pour les hôtes  
  Ajouter la ligne suivante dans le fichier

```bash
/mnt/storage/dynamic XXX.XXX.XXX.0/24(rw,sync,no_root_squash,no_all_squash)
```

*note* : la plage d'adresse peut être modifiée pour être plus restrictive [Voir Doc](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/storage_administration_guide/nfs-serverconfig)

* Valider la modification du fichier

```bash
sudo exportfs -a
```

* Redémarrer le serveur nfs

```bash
sudo systemctl restart nfs-kernel-server
```

:warning: il est possible que le firewall interne bloque les ports nfs

Exemple avec ufw pour autoriser les connections nfs

```bash
sudo ufw allow from XXX.XXX.XXX.0/24 to any port nfs
```

## Montage des clients NFS

### Configurer les clients NFS

Sur chaques noeuds et masters le paquets `nfs-common` doit être installé pour que les hôtes puissent être client du serveur NFS

```bash
sudo apt-get update
sudo apt-get install nfs-common
```

### Prerequis

* Installer helm (un paquet manager) sur kubernetes (master)

```bash
cd /tmp
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > install-helm.sh
```

* Rendre le fichier executable

```bash
chmod u+x install-helm.sh
```

* Lancer l'installation

```bash
./install-helm.sh
```

* Créer le compte tiller qui sera utiliser par helm

```bash
kubectl -n kube-system create serviceaccount tiller
```

* Ajouter les droits au compte tiller pour qu'il ait accès au cluster

```bash
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```

* Lancer l'initialisation de helm pour s'installer sur le cluster

```bash
helm init --service-account tiller
```

* Vérifier que le pod tiller est déployé et `running`

```bash
kubectl get pods --namespace kube-system | grep tiller
```

## Deployer le NFS Provisionner sur Kubernetes

### Version simple (avec Helm)

* Sur le master k8s installer helm

```bash
helm install \
  --name nfs-client-provisioner \
  --set nfs.server=XXX.XXX.XXX.XXX \
  --set nfs.path=/mnt/storage/dynamic \
  --set storageClass.name=nfs \
  --set storageClass.defaultClass=true \
  stable/nfs-client-provisioner
```

* Modifier

* nfs.server : l'adresse ou fqdn du serveur NFS initialement crée
* nfs.path   : le point montage créer plus haut

### Version avancée (avec Helm + fichier values)

* Modifier le fichier values en conséquence puis lancer la commande suivante:

```bash
helm install --name nfs-client-provisioner -f values.yaml stable/nfs-client-provisioner
```

[Pour plus de détails](https://github.com/helm/charts/tree/master/stable/nfs-client-provisioner)

### Version deploiement à la dure (yaml)

* Récuper le dépot git

```bash
git clone https://github.com/kubernetes-incubator/external-storage
cd external-storage/nfs-client
```

* Suivre le tutorial [ici](http://teknoarticles.blogspot.com/2018/10/setup-nfs-client-provisioner-in.html)

OU directement

* Recuperer les fichiers dans le repératoir deploy dans ce repo

* Modifier le fichier deploy/class.yml afin de modifier le nom du provsionner

* Modifier le fichier deploy/deployment.yaml afin d'ajouter la configuration NFS (@ip et point de montage) ainsi que le nom du provsioner choisi

* Deployer les différents fichier

```bash
kubectl create -f deploy/rbac.yaml
kubectl create -f deploy/class.yaml
kubectl create -f deploy/deployment.yaml
```

### Test & Validation (quelques soit la méthode de déploiement)

* Vérifier la création de la storageClass:

```bash
kubectl get sc
```

* Créer un PVC qui va utiliser la storageClass (fichier pvc.yaml présent dans le  dossier /deploy du repo)

```bash
kubectl apply -f pvc.yaml
```

* Vérifier la création du PVC

```bash
kubectl get pvc
```

* Créer un POD (busybox) qui utilise le PVC crée, à partir du dossier deploy toujours

```bash
kubectl apply -f pod.yaml
```

* Vérifier la création du pod

```bash
kubectl get pod
```

* Vérifier que le provisionner a créer un volume pour le pod

```bash
kubectl get pv
```

* Vérifier que le volume est effectivement persisté dans le stockage nfs  
  Pour cela, se positionner sur la machine NFS et vérifier le dossier (ici mnt/storage/dynamic)  
  Un dossier commencer par `default-test-claim-` doit exister.

## Fonctionnement général

Voici le schéma de fonctionnement générale

![Image dynamic provi](./images/NFS_Dynamic_Provisioning.png)

Lors d'un déploiement classique via des fichiers yaml, un Pod ou Déploiement monte un volume en se basant sur un PVC (Persistent Volume Claim).  
Ce PVC est spécifique à chaque déploiement et contient les exigences (taille du stockage) pour ce déploiement.  
Ce PVC se base sur une classe de stockage (storage Class) commune à tout le cluster et qui va permettre de provisionner des volumes persistents.  
Le volume existe alors en tant qu'objet sur Kubernetes et dans le point de montage NFS.  

Voici le schéma de fonctionnement général détaillé:

![Process dynamic provi](./images/volumes.png)

Lors ce qu’on détruit un PVC, il existe 3 états possibles concernant le PV associé.  
La définition de la politique de « réclamation » d’un PV est défini sur la classe de stockage.  

**Delete**
Cela veut dire que le PV ainsi que l’actif de stockage de l’infrastructure externe sont supprimés.  
Il est possible selon le provisionner de volume d’ajouter un paramètre d’archivage pour que l’actif de stockage ne soit pas totalement supprimé.  

**Recycle**
Va permettre de nettoyer le volume (rm –rf /LeVolume/*) et va le rendre disponible pour un nouveau PVC.

**Retain**
Une fois le PVC supprimé, le PV associé va être dans un état « Released » ce qui n’autorise aucun nouveau PVC de le réclamer.  
Afin de le réclamer, l’opération est manuelle.  

### Particularités

* Lors d'un déploiement avec helm il est possible (souvent) que le fichier values ne demande pas de PVC mais directement le nom de la classe de stockage (storageClass) dans ce cas, le PVC est créer automatiquement lors de l'installation par helm.
