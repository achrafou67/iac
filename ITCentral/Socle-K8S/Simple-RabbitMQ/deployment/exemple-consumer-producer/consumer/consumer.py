import logging
import pika
import sys
import time
import os

LOGGER = logging.getLogger(__name__)

class Consumer():

    def __init__(self, queue_name):
        self._connection = None
        self._channel = None
        self._consumer_tag = None
        self._host = os.environ['RABBITMQ_HOST']
        self._port = os.environ['RABBITMQ_PORT']
        self._vhost = os.environ['RABBITMQ_VHOST']
        self._exchange = os.environ['RABBITMQ_EXCHANGE']
        self._queue = queue_name
        self._routing_key = self._queue
        self._credentials = pika.PlainCredentials(os.environ['RABBITMQ_USER'], os.environ['RABBITMQ_PASS'])

    def init(self):
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(self._host, self._port, self._vhost, self._credentials))
        self._channel = self._connection.channel()
        self._channel.exchange_declare(exchange=self._exchange, exchange_type='direct')
        self._channel.queue_declare(queue=self._queue)
        self._channel.queue_bind(queue=self._queue, exchange=self._exchange, routing_key=self._routing_key)

    def callback(self, ch, method, properties, body):
        message_received = body.decode('utf-8')
        print("LISTENING ON %s" % self._queue)
        if message_received == "hello":
            print ('[X] Tu as reçu un coucou !')
        elif message_received == 'FINISHED':
            self.stop()
        else:
            print ('[X] Tu as reçu un autre message : %r' % message_received)
        #ch.basic_ack(delivery_tag = method.delivery_tag)

    def run(self):
        self.init()
        self._consumer_tag = self._channel.basic_consume(self._queue, self.callback, auto_ack=True)
        self._channel.start_consuming()

    def stop(self):
        self._channel.queue_unbind(self._queue, exchange=self._exchange, routing_key=self._routing_key)
        self._channel.queue_delete(queue=self._queue)
        self._connection.close()
        self._channel.basic_cancel(self._consumer_tag)
        self._channel.close()
        self.reinit()

        print("Stoped")

    def reinit(self):
        self._channel = None
        self._connection = None
        self._queue = None
        self._exchange = None

