import sys
import logging
import time
from multiprocessing import Process
from consumer import Consumer

LOGGER = logging.getLogger(__name__)


class App():

    def start_consumer(self, queue_name):
        c = Consumer(queue_name)
        try:
            c.run()
        except (KeyboardInterrupt):
            c.stop()

    def run_consumer_process(self, queue_name):
        self._p = Process(target=self.start_consumer, args=(queue_name,))
        self._p.start()


if __name__ == '__main__':
    #app = App()
    #app.run_consumer_process("Consumer1")
    #app.run_consumer_process("Consumer2")
    app = App()
    if (len(sys.argv) > 3 ) or (len(sys.argv) < 2):
        print("usage : python3.7 app.py nb_consumer")
    else:
        for i in range(1, int(sys.argv[1])+1):
            app.run_consumer_process("Consumer"+str(i))
