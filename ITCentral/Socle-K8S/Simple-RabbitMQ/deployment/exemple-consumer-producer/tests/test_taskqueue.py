import unittest
from taskqueue import TaskQueue


class TestTaskQueue(unittest.TestCase):
    def setUp(self):
        self.queue = TaskQueue()

    def tearDown(self):
        pass
    
    def test_add_task(self):
        # Test add function
        self.queue.add_task('hello')

        # Test if the size is one
        self.assertEqual(self.queue.qSize(), 1)
      
        # Add differents kinds of  elements
        self.queue.add_task(1)
        self.assertEqual(self.queue.qSize(), 2)

        self.queue.add_task("test")
        self.assertEqual(self.queue.qSize(), 3)

        json = {'id':2}
        self.queue.add_task(json)
        self.assertEqual(self.queue.qSize(), 4)
       
    def test_get_task(self):
         # Test get function (with removing of the content)
         self.queue.add_task("hello")
         self.assertEqual(self.queue.get_task(), 'hello')
         self.assertEqual(self.queue.qSize(), 0)

         self.queue.add_task(2)
         self.assertEqual(self.queue.get_task(), 2)
         self.assertEqual(self.queue.qSize(), 0)


         data_json = { 
                      'id':1, 
                      "items":
                        ["item1","items2"]
                     } 

         self.queue.add_task(data_json)
         self.assertEqual(self.queue.get_task(), data_json)
         self.assertEqual(self.queue.qSize(), 0)
  
         self.assertTrue(self.queue.isEmpty())


if __name__ == "__main__":
    unittest.main()
