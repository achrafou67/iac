# queue.py
from multiprocessing import Queue

class TaskQueue():

  def __init__(self):
      'Declaring a FIFO Queue'
      self._taskqueue = Queue()

  def add_task(self, data):
      'Add a new data task'
      self._taskqueue.put(data)

  def get_task(self):
      'Get the first task in the FIFO'
      while self._taskqueue:
          return self._taskqueue.get(1)
      raise KeyError('Getting from an empty priority queue')

  def isEmpty(self):
      'Check if queue is empty'
      if (self.qSize() > 0):
          return False
      return True

  def qSize(self):
      return self._taskqueue.qsize()
