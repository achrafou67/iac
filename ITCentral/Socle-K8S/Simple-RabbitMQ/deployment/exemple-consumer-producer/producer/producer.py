import logging
import pika
import sys
import time
import os

LOGGER = logging.getLogger(__name__)

class Producer():
    EXCHANGE_TYPE = 'direct'
    ROUTING_KEY = ''

    def __init__(self):
        self._connection = None
        self._channel = None
        self._consumer_tag = None
        self._host = os.environ['RABBITMQ_HOST']
        self._port = os.environ['RABBITMQ_PORT']
        self._vhost = os.environ['RABBITMQ_VHOST']
        self._exchange = os.environ['RABBITMQ_EXCHANGE']
        self._credentials = pika.PlainCredentials(os.environ['RABBITMQ_USER'], os.environ['RABBITMQ_PASS'])


    def init(self):
        self._connection = pika.BlockingConnection(pika.ConnectionParameters(self._host, self._port, self._vhost, self._credentials))
        self._channel = self._connection.channel()
        self._channel.exchange_declare(exchange=self._exchange, exchange_type='direct')

    def publish(self, message, routing_key):
        self.init()
        message = message
        TYPE = "MSG"
        self._channel.basic_publish(exchange=self._exchange, routing_key=routing_key, body=message, properties=pika.BasicProperties(type=TYPE))


    def stop(self):
        self._connection.close()

def main(message, routing_key):
    p = Producer()
    p.publish(message, routing_key)
    p.stop()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("utilisation : python3.7 producer.py message routing_key")
    else:
        main(sys.argv[1], sys.argv[2])

