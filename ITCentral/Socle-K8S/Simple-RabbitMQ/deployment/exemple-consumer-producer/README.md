# Exemple de Producer/Consumer RabbitMQ


## Introduction

Ce mini-projet implémente la logique producteur/consommateur dans le cadre d'un système de message à file d'attente asynchrone (AMQP).
Cet implémentation utilise les bibliothèques suivantes:

[Pika](https://github.com/pika/pika)  
[MultiProcessing](https://docs.python.org/2/library/multiprocessing.html)


## Architecture du code

- D `consumer`: Répértoire contenant l'application maître et le code du consommateur
- D `producer`: Répértoire contenant le code du producteur
- F `vars`: Fichier contenant les variables d'environnement nécessaire pour le fonctionnement de RabbitMQ
- F `requirements.txt`: Fichier contenant les dépendances (pip3 install -r `requirements.txt` pour les installer)


## Folder Consumer
### app.py

Ce fichier permet de lancer un ou plusieurs consommateurs en utilisant des processus indépendants.

command:
```bash
python3.7 app.py nb_consumer
``` 

### consumer.py

Ce fichier contient toute la logique de consommation RabbitMQ.
Le cas d'usage est ici très simple.
Le consommateur écoute sur sa propre file d'attente* et lance une action en fonction du message reçu.
Dans ce programme, trois types de message sont possible:
- Un message contenant la chaine de caractères `hello`
- Un message contenant la chaine de caractères `FINISHED` (et qui interrompe le process et détruit la file)
- Toute autre messages ne contenant pas les deux messages ci-desus

*note* : La file d'attente porte le même nom que le nom donné au consommateur. De même la clé de routage porte le même nom.

### Folder Producer 
### producer.py

Ce fichier permet d'envoyer un message vers un consommateur.

command:
```bash
 python3.7 producer.py message routing_key
```


## TODO 

* Implémenter TaskQueues
* Implémenter les tests
* Dockerizer les microservices consumers et producers
* Documentation installation RabbitMQ sur K8S

## Developers
Achraf BENTABIB
- achraf.bentabib@hotmail.fr

