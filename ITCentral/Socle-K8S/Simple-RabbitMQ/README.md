## Objectif

L'objectif est de pouvoir bénficier d'un serveur RabbitMQ fonctionnel sur un cluster Kubernetes.  
Le serveur RabbitMQ sera bindé avec le serveur LDAP d'un Active Directory.

## Prérequis

* Avoir un active directory en place, avec SSL.
* Avoir un cluster Kubernetes opérationnel
* Avoir installer helm sur le cluster
* Avoir un NFS Provisionner pour les volumes persistent
* Avoir un ingress controller Kong + API Gw

## Modifier le fichier values

* Récupérer le fichier `values` disponible dans le répertoire deployement, depuis la master k8s. [Cliquer ici pour le fichier values](./deployment/values)

* Prendez connaissance du fichier

* Dans la partie *global* modifer le nom de la `storageClass` selon celle créee par le NFS Provisionner

* Dans la partie *rabbitmq* modifier les valeurs de `username`et `password` selon vos choix

* Vérifier que dans la partie *rabbitmq.plugins* le plugins `rabbitmq_auth_backend_ladp` est déjà présent

* Dans la partie *rabbitmq.extraConfiguration* modifier les valeurs suivantes

> **auth_ldap.server.1** = L'adresse IP de votre seveur Active Directory  
> **auth_ldap.dn_lookup_base** = Le domaine de votre serveur Active Directory  
> **auth_ldap.user_dn_pattern** = Modifier par rapport à votre domaine Active Directory

* Dans la partie *rabbitmq.advancedConfiguration.rabbitmq_auth_backend_ldap.tag_queries* :
* Modifier pour chaque rôle (`management`, `administrator`, `monitoring`) le **DistinguishedName** selon le group et ou crée dans votre AD

* Dans la partie *rabbitmq.advancedConfiguration.rabbitmq_auth_backend_ldap.vhost_access_query*
* Modifier la valeur du **DistinguishedName** selon les groupes et l'OU crée dans l'AD

 :warning: Pour chaque Vhost crée dans l'AD (exemple: **rabbitmq-vhostdev**) il faudra manuellement creer le vhost **vhostdev** sur RabbitMQ, voir la partie *Création d'un vhost*

* Dans la partie *peristence* modifier la valeur de `storageClass` selon celle créee par le NFS Provisionner

* Dans la partie *ingress* modifier la valeur du `hostName` selon votre choix

## Deploiement Helm

* Une fois que votre fichier *values* concorde avec vos besoins, lancer la commande suivante:

```bash
helm install --name rabbitmq-dev -f values.yaml stable/rabbitmq
```

* Vérifier que le pod rabbitmq est `running`

```bash
kubectl get pod
```

## Création du secret TLS

[Suivre le tutoriel](../kubernetes/README.md)

:warning: Le nom du secret doit correspondre à celui mis dans le fichier *values*.

## Validation

Une fois la création du secret TLS et la ressource ingress à jour, il est désormais possible d'accéder à l'interface RabbitMQ en se rendant se rendant sur le hostname crée pour l'Ingress.
Vous pouvez vous connecter avec le compte local paramétré dans le fichier *values*.

## Validation authentification AD

### Configuration AD

Pour faire fonctionner l'authentication LDAP il faut à présent

* Créer un utilisateur LDAP
* Créer les groupes management/monitoring/administrator dans un OU rabbitmq (en fonction de vos paramètre du fichier values)
* Ajouter l'utilisateur dans le groupe `management` (pour l'accès à la GUI)
* Créer un groupe vhost en respecant le format décrit dans le fichier yaml, dans une OU vhost (en fonction de vos paramètre du fichier values)
* Ajouter le group vhost à l'utilisateur

### Créer un vhost dans rabbitMQ

* Depuis le master, executer un shell dans le pod rabbitmq:

```bash
kubectl exec -it rabbitmq-dev-0 -* sh
```

* Créer un vhost

```bash
rabbitmqctl add_vhost nom_du_vhost
```

* Quitter le shell

```bash
Ctrl + D
```
