# Objectif

Mettre à jour docker sur un cluster Kubernetes.
Cette proécdure n'est pas utile si votre cluster n'utilise pas docker en tant que moteur d'exécution de conteneur.

# Introduction

Cette procédure décrit les opérations à effectuer sur un Cluster Kubernetes afin de mettre à jour docker sur une version spécifique..

Pour vérifier la version docker utilisée:

Commande:
```
docker version
```

**Warning**: L'ensemble des étapes sont à reproduire sur l'ensemble des nœuds du plan de contrôle et du plan de données du cluser. (master et worker)

Pour récupérer la liste des nœuds du cluster lancer la commande suivante sur un nœud du plan de contrôle:

```
kubectl get nodes
```

## Mise d'un nœud en maintenance

Cette étape est à effectuer sur un nœud du plan de contrôle.

Afin de d'effectuer une mise à jour sur un nœud, il faut tout d'abord le sortir du cluster.
Pour cela, il faut désactiver la planification de nouveaux déploiements sur le nœud en question.

```
kubectl cordon <NODE_NAME>
```

Ensuite, il faut expulser en toute sécurité les pods de ce nœud avant d'effectuer la maintenance sur le nœud.

```
kubectl drain <NODE_NAME> --ignore-daemonsets --delete-local-data –force
```

## Mise à jour sur le nœud

Pour mettre à jour la version docker, les opérations nécessaires sont les suivantes sur le nœud en maintenance:

Désactiver les services docker et kubelet
```
service kubelet stop
docker rm -f $(docker ps -aq)
service docker stop
```

Puis, supprimer les paquets docker

```
apt remove -y docker-ce docker-ce-cli containerd
```

Ensuite, il faut effectué une sauvegarde du démon docker, qui serait réutilisé.

```
mv /etc/docker/daemon.json /tmp
```

On peut désormais installer la version de docker que l'on souhaite.
Pour obtenir la liste des versions docker disponible on peut lancer la commande:

```
apt-cache madison docker-ce
```

Une fois la version choisie, on peut effectuer l'installation des composants nécessaires.

 ```
 VERSION=5:19.03.0~3-0~debian-buster
 apt install docker-ce=${VERSION} docker-ce-cli=${VERSION} containerd.io
```

On peut maintenant redéplacer le démon docker
```
mv /tmp/daemon.json /etc/docker/daemon.json
```

Et relancer les services des démons

```
systemctl daemon-reload
```

Une fois relancé, il ne reste plus qu'à relancer les services docker et kubelet.

```
service docker start
service kubelet start
```

## Terminer une maintenance d'un nœud

Une fois la maintenance du nœud effectué, il faut prévenir Kubernetes que le nœud peut être à nouveau utilisé.

Pour cela, sur le plan de contrôle lancer la commande:

```
kubectl uncordon <NODE_NAME>
```
