# Objectif

Mettre à jour Kubernetes afin qu'il puisse utiliser utiliser un runtime de conteneur différent de docker.  


# Introduction

Kubernetes désapprouve Docker en tant que runtime de conteneur après la v1.20.

Don't Panic 😱 

Les conteneurs Docker sont toujours pris en charge, mais le dockershim / Docker, la couche entre Kubernetes et containerd est obsolète et sera supprimée de la version 1.22+.

Il vaut mieux donc s-y préparer à l'avance.
Le choix se porte en général sur containerd comme moteur d'exécution des containers lorsque l'on utilisait docker, puisqu'il tourne déjà sur les noeuds du cluster Kubernetes. (*Docker utilise containerd*)

L'objectif de la migration est donc d'apporter à Kubernetes moins de dépendances avec Docker, abstrayant ainsi tout les couches de traduction entre les composants (dockershim, docker, containerd...)

Schéma haut niveau de migration:

![Migration](./K8s_migration.png)

## Vérifications

Sur un noeud du plan de contrôle vérifier quel container runtime est en lancement.

```
kubectl get nodes -o wide
```

On peut ensuite vérifier les namespaces de containerd avec sa CLI. (en root)

```
ctr namespace list
```

*moby* est le namespace de docker.

Puis on peut lister les containers qui tourne dans ce namespace.

```
ctr --namespace moby container list
```

Si tout va bien jusqu'ici alors bonne nouvelle, on pourra changer le CRI utilisé par Kubernetes.

Les opérations seront effectué sur chaque nœud de cluster, en commençant par les nœuds du data plane et en finissant avec les nœuds du control plane.

## Mise d'un nœud en maintenance

Cette étape est à effectuer sur un nœud du plan de contrôle.

Afin de d'effectuer une mise à jour sur un nœud, il faut tout d'abord le sortir du cluster.
Pour cela, il faut désactiver la planification de nouveaux déploiements sur le nœud en question.

```
kubectl cordon <NODE_NAME>
```

Ensuite, il faut expulser en toute sécurité les pods de ce nœud avant d'effectuer la maintenance sur le nœud.

```
kubectl drain <NODE_NAME> --ignore-daemonsets
```

ou bien si une erreur survient
```
kubectl drain <NODE_NAME> --ignore-daemonsets --delete-local-data –force
```

Le noeud passe en statut `SchedulingDisabled`

On peut le vérifier avec la commande suivante.

```
kubectl get nodes
```
## Maintenance du noeud
### Activation plugin CRI de containerd

Pour mettre à jour le CRI docker, les opérations nécessaires sont les suivantes sur le nœud en maintenance:

Désactiver les services docker et kubelet
```
service kubelet stop
docker rm -f $(docker ps -aq)
service docker stop
```

Puis, supprimer les paquets docker (optionel)

```
apt remove -y docker-ce docker-ce-cli
apt purge docker-ce docker-ce-cli
```

Ensuite il faut commenter la ligne *disabled_plugins* dans le fichier `/etc/containerd/config.toml` afin d'activer le plugin CRI de containerd.

Si ce fichier n'existe pas il convient le générer de la manière suivante.

```
containerd config default > /etc/containerd/config.toml
```

Enfin, nous pouvons redémarrer containerd.

```
systemctl restart containerd
```

### Changement du runtime

Afin de changer le moteur d'exécution docker, nous allons éditer le fichier 
`/var/lib/kubelet/kubeadm-flags.env` sur le noeud.

Puis, nous ajoutons les *flags* suivants:
- `--container-runtime=remote`
- `--container-runtime-endpoint=unix:///run/containerd/containerd.sock`

Le fichier devrait ressembler à quelque comme cela:

```
KUBELET_KUBEADM_ARGS="--cgroup-driver=systemd --network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.2
--resolv-conf=/run/systemd/resolve/resolv.conf --container-runtime=remote --container-runtime-endpoint=unix:///run/containerd/containerd.sock"
```


Après avoir changer le runtime, il suffit de redémarrer le service kubelet.

```
systemctl start kubelet
```

### Vérification

Sur le plan de contrôle relancer la commande de listing des noeuds.

```
kubectl get nodes -o wide
```

Si tout s'est bien passé le runtime devrait maintenant être containerd.


### Terminer une maintenance d'un nœud

Une fois la maintenance du nœud effectué, il faut prévenir Kubernetes que le nœud peut être à nouveau utilisé.

Pour cela, sur le plan de contrôle lancer la commande:

```
kubectl uncordon <NODE_NAME>
```

On peut aussi constater que sur le noeud qui était en maintenance qu'un namespace `k8s.io` est maintenant visible dans la liste des namespaces de containerd.


Il ne reste plus qu'à reproduire les opérations sur tous les noeuds de cluster.
