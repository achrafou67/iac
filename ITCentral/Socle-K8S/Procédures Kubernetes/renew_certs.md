# Objectif

Mettre à jour les certificats PKI d'un cluster Kubernetes

# Introduction

Cette procédure décrit les opérations à effectuer sur un Cluster Kubernetes afin de mettre à jour les certificats de la PKI Kubernetes.

Selon la version de Kubernetes, il est possible d'afficher les dates d'expirations de tout le certificat qui compose la PKI de Kubernetes.

Commande:
```
kubeadm certs check-expiration
```
ou 

```
kubeadm alpha certs check-expiration
```
Commande manuelle:

```
find /etc/kubernetes/pki/ -type f -name "*.crt" -print|egrep -v 'ca.crt$'|xargs -L 1 -t  -i bash -c 'openssl x509  -noout -text -in {}|grep After'
```

Pour information, les fichiers composants la PKI de Kubernetes se trouve dans le répertoire `/etc/kubernetes/pki` de chaque master (control plane).

## Mise à jour automatique lors d'un upgrade

Kubeadm renouvelle tout les certificats durant un upgrade de plan de contrôle.

Il est recommandé de mettre à jour le cluster fréquemment pour afin de rester sécurisé.

[Voir procédure de mise à jour d'un cluster](./upgrade_cluster.md)


**Important:** Si le cluster est mode HA (multi-master) les opérations de mise ç jour sont à effectuer sur tous les noeuds du plan de contrôle.

## Mise à jour manuel

Selon la version du cluster, deux méthodes peuvent être utilisée pour mettre à les certificats. 

### Commande renew

Afin de mettre à jour l'ensembles des certificats, la commande suivantes peut être utilisée:
`kubeadm certs renew` 

Cette commande effectue le renouvellement en utilisant le certificat d'autorité (CA or front-proxy-CA) et la clé sauvegardés dans le répétoire `/etc/kubernetes/pki`.

*Note: Cette méthode peut provoquer des erreurs, auquel cas la méthode manuelle ci-dessus est nécessaire.*

### Manuellement

La méthode mannuel consiste à relancer la génération de la PKI sur les noeuds du contrôle plane. Cette solution est surtout pour les versions 1.14 ou inférieur de Kubernetes.
Pour cela, il est recommandé de procéder à une sauvegarde la PKI existante:

```
mkdir ~/pki.backup
cd /etc/kubernetes/pki/
mv {apiserver.crt,apiserver-etcd-client.key,apiserver-kubelet-client.crt,front-proxy-ca.crt,front-proxy-client.crt,front-proxy-client.key,front-proxy-ca.key,apiserver-kubelet-client.key,apiserver.key,apiserver-etcd-client.crt} ~/pki.backup
```

Une fois la sauvegarde effectuée, il faut relancer la génération des tous les certificats grâce à la commande suivante:

**Warning**: L'IP à mettre est celle de la VIP montée sur le cluster HA, ou bien l'adresse IP du master dans le cas d'un cluster simple.

```
kubeadm init phase certs all --apiserver-advertise-address **IP**
```

Maintenant, il faut regénérer les fichiers de la config kube.
Pour cela, toujours sur le noeud du contrôle plane, faire une sauvegade.

```
mkdir ~/kubeconfig
cd /etc/kubernetes/
mv {admin.conf,controller-manager.conf,kubelet.conf,scheduler.conf} ~/kubeconfig
```

Puis lancer la phase d'initialisation pour configurer les fichiers kube, et redémarrer les services du master (soit docker, soit containerd, et kubelet). Sinon redémarrer le master.
```
kubeadm init phase kubeconfig all
systemctl restart docker && system restart kubelet
```


Une fois redémarré, il faut copier la nouvelle configuration admin vers le répertoire de config kube.

```
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
```

N'oubliez pas copier ce fichier sur les autres utilisateurs nécessitant un accès à kubectl, et de remettre les bons droits d'accès `chown`.
N'oubliez de faire un export de ce fichier vers la variable d'environnement `KUBE_CONFIG` via la commande  `export KUBECONFIG=$KUBECONFIG:$HOME/.kube/config`.
