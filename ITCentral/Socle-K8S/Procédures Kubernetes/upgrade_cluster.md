## Objectif

Mettre à jour Kubernetes suivant les indications du site officiel.

# Mettre à jour un cluster Kubernetes

Cette procédure décrit les opérations à effectuer sur un Cluster Kubernetes afin de mettre à jour sa version.

La procédure définit les activités à effectuer dans l'ordre:
1. Mettre à jour le noeud du plan de contrôle principal
2. Mettre à jour les noeuds du plan de contrôle additionnels
3. Mettre à jour les noeuds du plan de données.  

## Prérequis

- Swap est désactivé (swapoff -a)
- Lire les notes de la version à installer attentivement.
- Effectuer un backup si nécesaire des composants curciauw


## Informations +

- L'opération de mise à jour entraîne le redémarrage de tout les containers.
- La mise à jour ne se fait que d'une version MINOR à la version MINOR qui suit, ou entre des versions PATCH d'une même version MINOR.
- Les commandes sont à effectué en root pour la plus part des cas (installation de packet etc...), dans les autres cas avec un utilsiateurs ayant accès à `kubectl`

## Determiner la version à installer.

Les versions peuvent être trouvées grâce à la commande suivante:  
```bash
apt update
apt-cache madison kubeadm
```

Les versions sont écrites de cette façon: `MAJOR.MINOR.PATCH-00`  
Dans les commandes qui suivent, remplacer la valeur  MAJOR.MINOR.PATCH-00 par celle de votre plan de mise à jour.

## Mettre à jour les noeuds du plan de contrôle

### Noeud principal du plan de contrôle 

En root sur la machine principale du plande contrôle lancer les commandes suivantes:  
```bash
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=MAJOR.MINOR.PATCH-00 && \
apt-mark hold kubeadm
```

Vérifier que la version de kubeadm est la bonne avec la commande suivante: 
```bash
kubeadm version
````

Puis lancer la commande, et prenez note des informations.
```
kubeadm upgrade plan
```

Puis lancer la commande 
```
kubeadm upgrade apply vMAJOR.MINOR.PATCH-00
```

### Noeuds secondaires

Tâche à faire, sur chaque noeud secondaire du plan de contrôles.

Mettre à jour kubeadm.
```bash
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=MAJOR.MINOR.PATCH-00 && \
apt-mark hold kubeadm
```
  
Puis lancer la mise à jour de la configuration kubelet  

```bash
kubeadm upgrade node
```

Les prochaines étapes doivent être effectuées sur les noeuds du plan de controles, l'un après l'autre.

### Mise en maintenance des noeuds du plan de contrôles 


Pour mettre le noeud en maintenance:
```bash
kubectl drain <cp-node-name> --ignore-daemonsets
```

### Mise à jour kubelet et kubectl

Sur le noeud en maintenance lancer les commandes suivantes:

```bash
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.x-00 kubectl=1.19.x-00 && \
apt-mark hold kubelet kubectl
```

Puis redémarrer les services  

```
systemctl daemon-reload
systemctl restart kubelet
```

### Finir la maintenance

Une fois kubelet et kubectl mis à jour et redémarrer retourner sur un noeud du master puis remettre le noeud en fonction.

```bash
kubectl uncordon <cp-node-name>
```

Les prochaines sont à effectué sur les noeuds du plan de données l'un après l'autre.  

## Mettre à jour les noeuds du plan de données

### Mettre à jour Kubeadm

Sur le noeud lancer:  
```bash
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.19.x-00 && \
apt-mark hold kubeadm
```

### Mettre en maintenance le noeud

Sur une machine du plan de contrôle lancer mettre le noeud en maintenance:

```bash
kubectl drain <NODE_NAME> --ignore-daemonsets --delete-local-data
```

## Mise à jour kubelet et kubectl 

Sur le noeud en maintenance lancer les commandes suivantes:

```bash
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.x-00 kubectl=1.19.x-00 && \
apt-mark hold kubelet kubectl
```

Puis redémarrer les services  

```
systemctl daemon-reload
systemctl restart kubelet
```

### Finir la maintenance

Une fois kubelet et kubectl mis à jour et redémarrer retourner sur un noeud du master puis remettre le noeud en fonction.

```bash
kubectl uncordon <cp-node-name>
```


# Vérifier les versions:

```bash
kubectl get nodes -o wide
```


ET voilà ! Cluster mit à jour :) 
