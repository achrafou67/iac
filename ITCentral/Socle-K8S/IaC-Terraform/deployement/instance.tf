data "vsphere_datacenter" "dc" {
  name = var.vs_dc
}

data "vsphere_datastore" "datastore" {
  name = var.vs_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name = var.vs_resource_pool
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name = var.vs_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name = var.vs_template
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "vm" {
  count = var.vm_number
  name = format("%s-%s", var.vm_name_prefix, (count.index + 1))
  resource_pool_id = data.vsphere_resource_pool.pool.id
  folder = var.folder
  
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus= var.vm_vcpu_num
  memory = var.vm_ram_size
  guest_id = data.vsphere_virtual_machine.source_template.guest_id

  scsi_type = data.vsphere_virtual_machine.source_template.scsi_type

  network_interface {
    network_id = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.source_template.network_interface[0]
  }

  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = format("%s-%s", var.vm_name_prefix, (count.index + 1))
        domain = var.vm_domain
      }

      network_interface {
        ipv4_address = format("%s%s", var.vm_network_base, (var.vm_first_host + count.index + 1))
        ipv4_netmask = var.vm_netmask
      }

      ipv4_gateway = var.vm_gateway
      dns_suffix_list = [var.vm_domain]
      dns_server_list = var.vm_dns_server

    }
  }
}

