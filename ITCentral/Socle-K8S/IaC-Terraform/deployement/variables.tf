variable "vs_user" {
  description = "Nom de l'utilisateur de l'API vSphere"
}

variable "vs_pass" {
  description = "Mot de passe associé à l'utilsiateur"
}

variable "vs_server" {
  description = "Nom de domaine du serveur vSphere"
}

variable "vs_dc" {
  description = "Nom du Datacenter vSphere"
}

variable "vs_datastore" {
  description = "Nom du datastore vSphere"
}

variable "vs_resource_pool" {
  description = "Nom du pool de resource vSphere"
}

variable "vs_network" {
  description = "Nom du réseau vSphere (Dportgroup)"
}
variable "vs_template" {
  description = "Nom de du template de VM"
}

variable "vm_folder" {
  description = "Nom du dossier ou va se trouve la VM"
}

variable "vm_number" {
  description = "Nombre de VM à créer"
}
variable "vm_name_prefix" {
  description = "Prefix du nom de la VM"
}
variable "vm_vcpu_num" {
  description = "Nombre de vCPU à allouer"
}
variable "vm_ram_size" {
  description = "Quantité de RAM à allouer"
}
variable "vm_domain" {
  description = "Nom de domaine la VM"
}
variable "vm_network_base" {
  description = "Base de l'adresse IP sur 3 octets"
}
variable "vm_netmask" {
  description = "CIDR du masque réseau"
}
variable "vm_gateway" {
  description = "Passerelle par défaut"
}
variable "vm_first_host" {
  description = "Dernier octet de la dernière adresse IP utilisée"
}
variable "vm_dns_server"{
  description = "Liste des serveurs DNS"
}

