provider "vsphere" {
  user = var.vs_user
  password = var.vs_user
  vsphere_server = var.vs_server

  allow_unverified_ssl = true
}

