## Objectifs

- Les scripts de se projet Terraform permetent de provisionner de la resource virtuelle (VM) sur vSPhere en utilisant un modèle de VM déjà construit.

## Prérequis

- Créer un modèle de VM Debian, puis
  - Activer la connexion par SSH pour le root dans `/etc/ssh/sshd_config` (PermitLogin Yes)
  - Modifier le guestOS dans les paramètres de la VM en Ubuntu Linux 64
  - Installer open-vm-tools
  - Installer `net-tools 1.60-26ubuntu` avec dpkg
    - [Package à télécharger et installer](http://cz.archive.ubuntu.com/ubuntu/pool/main/n/net-tools/net-tools_1.60-26ubuntu1_amd64.deb)
  - Créer un lien symbolique pour DHCP : `ln -s /etc/dhcp /etc/dhcp3`
  - Ajouter `After=dbus.service` dans la partie `[Init]` dans `/lib/systemd/system/open-vm-tools.service`
  - Supprimer les lignes concernant l’interface ens192 dans le fichier `/etc/network/interfaces`
- Installer terraform avec apt, sur une machine virtuelle connectée à vSphere

## Déploiement

### Initialisation du projet Terraform

Sur le terminal, lancer la commande suivante :

```bash
terraform init
```

*Note*: Corrigez les éventuels erreurs si cette commande retourne un résultat négatif.

### Plannifiez et vérfier le dépoiement

Sur le terminal, lancer la commande suivante :

```bash
terraform plan
```

### Appliquer le déploiement

```bash
terraform apply
# Ne pas oubliez ne mettre "yes" à la question de sécurité.
```

*Note*: Corrigez les éventuels erreurs si cette commande retourne un résultat négatif.

### Variables

- Dans le fichier `terraform.tfvars`, on retrouve les variables suivantes à modifier selon l'infra:

 | Variable | Exemple de valeur | Description |
 | ------------------------ | ------------- | ------- |
 | vs_user | "firstname.lastname@domain" | Compte de service ayant les droits sur l'API vSphere |
 | vs_pass | "password" | Mot de passe associé au compte de service |
 | vs_server | "vcenter.domain" | Nom de domaine du serveur vSphere |
 | vs_dc | "DC-1" | Nom du datacenter vSphere |
 | vs_datastore | "Datastore-DC-1" | Nom du datastore vSphere |
 | vs_resource_pool | "pool-dc" | Nom du pool de ressources |
 | vs_network | "DPortGroup-DC-1" |  Nom du réseau de VM (vSwitch, DPortGroup, ...)|
 | vs_template | "template-debian" | Nom du modèle de VM à utiliser |
 | vm_folder | "Folder" | Répértoire de destination de la VM |
 | vm_number | 6 | Nom de machine virtuelle à provisioner|
 | vm_name_prefix | "new-vm-" | Préfix du nom de la VM|
 | vm_vcpu_num | 2 | Nombre de vCPU |
 | vm_ram_size | 4096 | Quantité de RAM |
 | vm_domain | "domain" | Domaine de l'hôte de la VM |
 | vm_network_base | "x.x.x." | Trois premiers octet de l'adresse IP |
 | vm_first_host | 1 | Quatrième octet de l'adresse IP, la première VM sera créer à N+1|
 | vm_netmask | 24 | CIDR |
 | vm_gateway | "x.x.x.x" | Paserelle par défaut de la VM |
 | vm_dns_server| ["x.x.x.x", "8.8.8.8"] | Liste des serveurs DNS |

### Etendre la taille du disque pour des machines non LVM

```bash
apt -y install parted
parted /dev/sda resizepart 2 100%
parted /dev/sda resizepart 5 100%
pvresize /dev/sda5
lvresize --extents +100%FREE --resizefs /dev/debian-vg/root
```
