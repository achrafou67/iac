## Objectif

Deployer l'ingress controller qui va récupérer les resources ingress crée.
Exposer l'ingress controller comme un service proxy.

## Fonctionnement générale d'un ingress controller

![Image ingress controller](./images/ingress_controller.png)

*Note: L'ingress controller joue également ici le rôle du Proxy.*

## Installation

Prérequis:

- Avoir déployer un NFS provisioner

### Deploiement yaml

- Se connecter sur le master K8S, user `ansible`

- Autoriser le user sur docker

```bash
kubectl apply -f all-in-one-postgres-vct_static_nodeport.yaml
```

sudo usermod -aG docker ansible

- Récupérer le fichier de déploiement `deploy.yaml` sur le master K8S (user ansible)

- Déployer le fichier

```bash
kubectl apply -f deploy.yaml
```

- Vérifier le déploiement

```bash
kubectl get pods -n kong
```

- Créer des variables d'environnement pour récupérer l'adresse IP et le NodePort de kong

```bash
export KONG_PROXY_IP=$(kubectl get nodes --namespace default -o jsonpath='{.items[0].status.addresses[0].address}')
export KONG_PROXY_PORT=$(kubectl get svc --namespace kong kong-proxy  -o jsonpath='{.spec.ports[0].nodePort}')
export KONG_PROXY_PORT_SSL=$(kubectl get svc --namespace kong kong-proxy  -o jsonpath='{.spec.ports[1].nodePort}')
```

## Utilisation

### Deployer une application et un service (exemple simple en ligne de commande)

- Déployer une application simple Hello world (google)

```bash
kubectl create deployment hello-node --image=asbubam/hello-node
```

- Vérifier le déploiement

```bash
kubectl get deployments
```

- Vérifier le pod

```bash
kubectl get pods
```

- Créer un service pour le pod

```bash
kubectl expose deployment hello-node --type=ClusterIP --port=8080
```

- Vérifier le service crée

```bash
kubectl get svc
```

### Affecter une ressource ingress pour le service

- Deployer l'ingress pour le service hello-node créer auparavant

```yaml
cat <<EOF | kubectl apply -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: hello
  annotations:
    kubernetes.io/ingress.class: "kong"
spec:
  rules:
  - host: hello.dev.test
    http:
      paths:
      - backend:
          serviceName: hello-node
          servicePort: 8080
EOF
```

- Vérifier que la ressource ingress a été detecter par l'ingress controller

```bash
kubectl get ingress
```

La ressource doit avoir une adresse IP d'un noeud du cluster.

- Vérifier que le fonctionnement de l'ingress controller

```bash
curl $KONG_PROXY_IP:$KONG_PROXY_PORT -H "host: hello.dev.test"
```

La commande renvoit `Hello World!`, l'ingress est fonctionnel.

### Securiser l'accès au service en utilisant HTTPS

#### Créer un plugin Kong pour la redirection HTTPS

- Installer une ressource kongPlugin pour rediriger les requêtes vers l'ingress controller SSL.
ATTENTION: Le plugin ne marche que pour un namespace !  
Un ingress dans un namespace A ne verra pas le plugin s'il est installé sur un namespace B.  

```bash
cat <<EOF | kubectl apply -f -
apiVersion: configuration.konghq.com/v1
kind: KongPlugin
metadata:
  name: https-redirect
  namespace: default
plugin: pre-function
config:
  functions:
  - |
    local scheme = kong.request.get_scheme()
    if scheme == "http" then
      local host = kong.request.get_host()
      local query = kong.request.get_path_with_query()
      local url = "https://" .. host ..query
      kong.response.set_header("Location",url)
      return kong.response.exit(302,url)
    end
EOF
```

- Modifier l'ingress du pod crée précedemment pour prendre kong la redirection SSL

```bash
kubectl patch ingress hello -p '{"metadata":{"annotations":{"configuration.konghq.com":"ingress-configuration", "plugins.konghq.com":"https-redirect"}}}'
```

- Vérifier le fonctionnement de l'ingress controller SSL

```bash
curl -kv https://$KONG_PROXY_IP:$KONG_PROXY_PORT_SSL -H "host: hello.dev.test"
```

## Mettre en place un HA Proxy devant le proxy kong

### Générer un certificat pour kong proxy

Sur la machine haproxy

- Récupérer le fichier `generate_certificates.sh`

-Créer un répértoire `coffre` dans le dossier `/opt`

```bash
mkdir -p /opt/coffre
```

- Lancer la génération du certificat

```bash
chmod +x generate_certificates.sh
./generate_certificates.sh api.devgalaxy.vp
```

*Note* : Le FQDN `api.devgalaxy.vp` doit être un alias de la machine sur laquelle est installé l'HAPROXY

- Installer Haproxy

```bash
sudo apt install haproxy
```

- Modifier le fichier `/etc/haproxy/haproxy.cfg` et ajouter les lignes suivantes (en fonctions de l'infra)

```yaml
frontend front-kong-proxy
        bind *:443 ssl crt /opt/coffre/api.devgalaxy.vp/api.devgalaxy.vp.pem
        option forwardfor
        redirect scheme https if !{ ssl_fc }
        mode http
        option httplog

        use_backend back-kong-proxy

backend back-kong-proxy
        mode http
        option httpclose
        balance roundrobin
        option forwardfor

        server kong1 master.devgalaxy.vp:31002 check ssl verify none
        server kong2 node1.devgalaxy.vp:31002 check ssl verify none
        server kong3 node2.devgalaxy.vp:31002 check ssl verify none
```

*Note* : Le port 31002 correspond au port SSL du PROXY KONG (variable d'environnement `$KONG_PROXY_PORT`) exportée plus haut.

### UTILISATION SANS DNS...(PERIMé)

Pour chaque service créer, l'ingress route par rapport un host précisé (exemple : `hello.dev.test`)  
Il faut donc renseigner ce nom d'hôte dans le PC qui souhaite accéder au service.  
Sur Windows modifier `C:\Windows\System32\drivers\etc`hosts` pour ajouter la correspondance  

```bash
@IP_Haproxy nom_hote_du_service
```
