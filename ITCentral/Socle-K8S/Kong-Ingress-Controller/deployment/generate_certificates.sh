#!/bin/bash



if [ $# -ne 1 ] ;
then
   echo "Il manque le FQDN"
   exit 0
fi


#Variables
path_coff="/opt/coffre"
path_work="$path_coff/$1"
params_cn="/CN=$1/emailAddress=achraf.bentabib@spie.com/O=SPIE CLOUD SERVICES/OU=DEV/C=FR/ST=FRANCE/L=PARIS"

echo "Chemin de travail : $path_work" echo "Chaine 'subject ssl' : $params_cn"

mkdir "$path_work"


# Generation de la clé privé du CA
openssl genrsa -out "$path_work/$1-root_ca.key" 4096
chmod 600 "$path_work/$1-root_ca.key"

# Génération du certificat PEM du CA
openssl req -x509 -new -nodes -key "$path_work/$1-root_ca.key" -sha256 -days 3650 -out "$path_work/$1-root_ca.pem" -subj "$params_cn"
chmod 600 "$path_work/$1-root_ca.pem"

# Extraction du certificat CRT du PEM
openssl x509 -outform der -in "$path_work/$1-root_ca.pem" -out "$path_work/$1-root_ca.crt"
chmod 600 "$path_work/$1-root_ca.crt"

# Génération du fichier de configuration CSR du CA
cat > $path_work/$1-root_ca.csr.conf <<-EOF
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=FR
ST=FRANCE
L=PARIS
O=SPIE CLOUD SERVICES
OU=DEV
emailAddress=sebastien.denichoux@spie.com
CN = $1
EOF

# Génération du fichier d'extensions
cat > $path_work/$1.v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $1
EOF

# Génération de la clé privé et le CSR pour le serveur
openssl req -new -sha256 -nodes -out "$path_work/$1.csr" -newkey rsa:2048 -keyout "$path_work/$1.key" -config <( cat $path_work/$1-root_ca.csr.conf )
chmod 600 "$path_work/$1.csr"
chmod 600 "$path_work/$1.key"

# Génération du certificat pour le serveur
openssl x509 -req -in "$path_work/$1.csr" -CA "$path_work/$1-root_ca.pem" -CAkey "$path_work/$1-root_ca.key" -CAcreateserial -out "$path_work/$1.crt" -days 3650 -sha256 -extfile "$path_work/$1.v3.ext"
chmod 600 "$path_work/$1.crt"

# Génération du certificat PEM
cat "$path_work/$1.crt" "$path_work/$1.key" > "$path_work/$1.pem"
chmod 600  "$path_work/$1.pem"

# Génération du fichier p12
openssl pkcs12 -export -inkey "$path_work/$1.key"  -in "$path_work/$1.pem" -name $1 -out "$path_work/$1.p12"
chmod 600 "$path_work/$1.p12"

# Affichage des fichiers crees
ls -iasl $path_work/

# Nettoyage SRL
rm $path_coff/*.srl



echo "Terminé"
