## Prérequis

Avoir déployer l'ingress controller et le proxy Kong ainsi que Konga
Suivre le [tutoriel ingress](../kong-ingress-controller/README.md)  
Suivre le [tutoriel konga](../konga/README.md)

- Exporter l'adresse ip de kong admin API

```bash
export KONG_API_IP=$(kubectl get nodes --namespace default -o jsonpath='{.items[0].status.addresses[0].address}')
export KONG_API_PORT=$(kubectl get svc --namespace kong kong-ingress-controller -o jsonpath='{.spec.ports[0].nodePort}')
```

L'API Admin est maintenant opérationnelle pour créer nos services/routes/consumer etc...  
L'idée est maintenant de plugger l'adresse `http://$KONG_API_IP:$KONG_API_PORT`en tant que connection sur l'outil Konga comme suit:

![Image konga connections](./images/konga_connections.png)

## Schéma global

![Image kong schema](./images/kong_schema.png)

### Fonctionnement

Lorsqu'un service est déployé et qu'il possède une ressource ingress, l'ingress controller va alors découvrir cet ingress.  
Une fois découvert le service est accessible par kong proxy. (comme dans le [tutoriel](https://gitlab.com/achrafou67/devops/tree/master/kong-ingress-controller))  

Au même moment l'admin API GW de Kong va découvrir le service connecté au Proxy.  
Il suffit alors pour l'admin (le devOps) d'intéragir avec l'API GW par exemple pour installer un plugin d'authentification.  
Il peut le faire directement en curl ou bien avec l'interface Konga.  
Konga étant raccordé à l'API GW, elle contiendra tout les services qui ont été découverts.  

## Sécuriser l'accès à Kong Admin

Dans l'installation décrit dans ce document, Kong Admin est exposé à travers un NodePort K8s.  
Cela rend donc l'accès au service ouvert à n'importe qui ayant accès à l'adresse du réseau du cluster.  
Kong Admin n'est pas par défaut sécurisée ce qui permet un accès à toute les méthodes portant sur son API.  

Pour éviter cette "faille", l'idée est de créer une `loopback` sur le service de Kong Admin.
Premièrement, il faut modifier le service pour qu'il soit exposer non plus en NodePort mais en ClusterIP.

- Pour cela, modifier le service kong-admin en lancant la commande suivante:

```bash
kubectl edit svc kong-ingress-controller -n kong
```

- Puis modifier le `type: Nodeport` en `type: ClusterIP` et supprimer les `nodePort` statiques.

- Ensuite créer un ingress comme suit (en modifidiant biensur le host selon votre infra)
*Note* Le plugin https-redirect a été déployé dans le namespace KONG

```bash
cat <<EOF | kubectl apply -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: kongadmin
  namespace: kong
  annotations:
    kubernetes.io/ingress.class: "kong"
    configuration.konghq.com : "ingress-configuration"
    plugins.konghq.com : "https-redirect"
spec:
  rules:
  - host: kongadmin.devgalaxy.vp
    http:
      paths:
      - backend:
          serviceName: kong-ingress-controller
          servicePort: 80
EOF
```

> En effectuant cet opération, on crée un ingress qui sera lu par l'ingress controller de Kong et ajouter aux routes de Kong Proxy.

- Une fois que l'ingress est monté, effectuer un test d'accès sur le master:

```bash
curl https://XXX.XXX.XXX.XXX:31002 -H "host: kongadmin.devgalaxy.vp"
```

Vérifiez que la commande retourne un résultat non nul.

> XXX.XXX.XXX.XXX:31002 correspond à l'adresse du service Kong Proxy en HTTPS (kubectl get svc -n kong)
> Note: le host kongadmin.devops.vp doit être rajouté au DNS et doit correspondre à l'IP de votre HAproxy.

- Maintenant que Kong Admin est devenu un service Kong, on peut se rendre Konga
  Il faut désormais désactiver la connection existante, et en créer une nouvelle avec l'adresse http utilisé dans le curl
  Puis activer la connection.

> Note la connexion sera refusée car Konga n'accepte par défaut les connexions TLS.
> Il faut se rendre sur le serveur ou est installé Konga, éteindre l'application (npm stop)
> Et ajouter dans le fichier /var/konga/konga/.env la variable `NODE_TLS_REJECT_UNAUTHORIZED = "0"`
> Puis rédémarrer le serveur Konga : npm start

- Dans le menu de gauche, sélectionner `Consumer`, créer un nouveau consomateur `kong_admin`

![Image create consumer](./images/create_consumer.png)

- Puis dans l'onglet `crédentials`, ajouter à cet utilisateur une API Key

![Image create api key](./images/create_api_key.png)

- Dans le menu de gauche sélectionner `Routes`, modifier la route correspondant à Kong-control-plane, en ajoutant un plugin `key-auth`

> Il faut seulement modifier la variable `key_names` et en lui ajoutant la clé `apiKey`

- Se rendre dans le menu `Connection` et modifier votre précédente connection en ajoutant l'API Key comme suit:

![Image add api key connection](./images/add_api_key_connection.png)

- Ajouter le `consumer` kong_admin dans un nouveau group `kong_admin`
- Dans la route kong-control-plane, ajouter un plugin `acl` et ajouter le groupe `kong_admin` dans la liste blanche.

Voilà ! Kong Admin est maintenant sécurisé.
