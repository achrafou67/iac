# Utilisation de certificats TLS dans le Cluster Kubernetes

| **SPIE ICS – Direction des Activités Spécialisées** |   |   |
| -- | :--: | -- |
| ISO27K-SMSI-Utilisation de certificats TLS dans le Cluster Kubernetes | RESTREINT | 12/03/2020 |

| ***Etat du document*** |                           |                         |                        |                              |
| ---------------- | ------------------------- | ----------------------- | ---------------------- | ---------------------------- |
| Provisoire       :white_large_square: | Pour Validation :white_large_square: | **Définitif** :white_check_mark: | Périmé :white_large_square: | A Supprimer :white_large_square: |

| ***Circuit de validation*** | Nom Prénom | Fonction | Date | Signature |
| --------------------------: | ---------- | -------- | :--: | :-------: |
| Rédaction | Achraf BENTABIB | Ingénieur R&D | 09/03/2020 | :white_check_mark: |
| Relecteur | Sébastien DENICHOUX | Responsable Développement | 12/03/2020 | :white_check_mark: |
| Relecteur | Emmanuel ANDRE | Responsable Infrastructure | 17/03/2020 | :white_check_mark: |
| Relecteur | Bogdan SZANTO | Responsable Centre de Compétences | 31/08/2020 | :white_check_mark: |
| Validation | Mathieu BICHON | RSSI | 07/10/2020 | :white_check_mark: |

## Objectif

Kubernetes fournit une API de provisionning de certificat TLS (certificates.k8s.io).  
L'autorité de certification est celle (par défaut) du cluster Kubernetes dans notre fonctionnement.  
L'idée est d'utiliser cette autorité pour créer nos certificats pour les services présents sur notre cluster  

## Introduction

Ce README fait référence au tutorial de kubernetes sur la management des certificats dans un cluster.  
La documentation complète se trouve sur [kubernetes](https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/)  

## Dépendances

Il est nécessaire d'installer [CFSSL](https://github.com/cloudflare/cfssl).

- Sur le master executer les commandes suivantes:

```bash
mkdir ~/bin
curl -s -L -o ~/bin/cfssl https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
curl -s -L -o ~/bin/cfssljson https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
chmod +x ~/bin/{cfssl,cfssljson}
export PATH=$PATH:~/bin
```

## Utilisation

### Etape 1

- Une fois CFSSL installé, on peut créer un répértoire pour le certificat pour un service

```bash
mkdir ~/service1
cd ~/service1
```

- Ensuite, créer un dossier dans lequel nous metterons les différents fichiers crées.

On génère une clé privé et une requête de signature de certificat (CSR)

```json
cat <<EOF | cfssl genkey - | cfssljson -bare server
{
  "hosts": [
    "monservice.namespace.svc.cluster.local",
    "monserviceIP.namespace.pod.cluster.local",
    "K8S Cluster IP",
    "POD Ip"
  ],
  "CN": "monserviceIP.namespace.pod.cluster.local",
  "key": {
    "algo": "ecdsa",
    "size": 256
  }
}
EOF
```

*Note1: Le nom du service est celui obtenu par la commande kubectl get service*  
*Note2: Le nom du namespace (default par défaut) est celui obtenu par la commande kubectl get service nom_service -o yaml*  
*Astuce: Pour obtenir l'IP du pod en question lancer la commande*

```bash
kubectl get pod rabbitmq-dev-0 --template={{.status.podIP}}
```

### Etape 2

On génère un object kubernetes pour lancer la demande crééé plus haut.

```yaml
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: monservice.namespace
spec:
  request: $(cat server.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF
```

### Etape 3

On approuve manuellement la requête

```bash
kubectl certificate approve monservice.namespace
```

### Etape 4

On récupère le certificat

```bash
kubectl get csr monservice.namespace -o jsonpath='{.status.certificate}' \
    | base64 --decode > server.crt
```

## Plus

### Créer un secret TLS a partir du certificat et de la clé privée

```bash
kubectl create secret tls monsecret --key server-key.pem --cert server.crt
```

> Attention: le secret doit se trouve dans le même namespace que le future Ingress  
> Pour spécifier le namespace pour la création du secret, ajoutez l'argument --namespace nom_namespace  

### Utiliser le secret dans un Ingress Kong

```yaml
cat <<EOF | kubectl apply -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: hello
  annotations:
    kubernetes.io/ingress.class: "kong"
    configuration.konghq.com : "ingress-configuration"

spec:
  tls:
  - hosts:
    - hello.dev.test
    secretName: monsecret
  rules:
  - host: hello.dev.test
    http:
      paths:
      - backend:
          serviceName: hello-node
          servicePort: 8080
EOF
```
