## Objectifs

- Les scripts de ce projet ansible permetent l'installation:
    - D'un serveur NFS
    - D'une configuration haproxy basique
    - Du keepalived entre deux machines
    - D'un cluster Kubernetes HA
    - D'un Ingress Controller (Kong)
    - D'un provisionner de volume (NFS Provisionner)
    - D'un dashboard Kubernetes 
    - D'un gestionnaire de package Kubernetes (Helm)

## Prérequis

- Deployer 8 VMs debian avec SSH avec la configuration système suivante:
    - Pour une configuration dev
        - Pour les 3 VM Master : 4 vCPU, 4Go de RAM, disque 30 Go
        - Pour les 3 VM Nodes :  2 vCPU, 2Go de RAM, disque 30 Go
        - Pour les 2 VM du cluster HAProxy : 1vCPU, 1 Go de RAM, 10Go
    - Pour une configuration prod
        - Pour les 3 VM Master : 8 vCPU, 8Go de RAM, disque 150 Go
        - Pour les 5 VM Nodes :  4 vCPU, 4Go de RAM, disque 150 Go
- Avoir une neuvième machine qui va jouer le rôle du configurateur Ansible
- Avoir une dixième machine qui va embarquer le serveur NFS
- Networking fait sur l'ensemble des machines, sur le même réseau

## Description du projet

## Architecture du programme

Le programme contient plusieurs fichiers/dossier importants:

* Les fichiers `inventory` Contient l'inventaire des machines distantes
* Le dossier `roles` contient tout les rôles utilisées dans ansible
* Le fichier `install_dependencies.yml` permet d'installer toutes les dépendences nécessaires
* Le fichier `deploy_ha_cluster.yml` permet de déployer le cluster
* Le fichier `deploy_nfs_server.yml` permet de déployer un serveur NFS
* Le fichier `deploy_haproxy.yml` permet une configuration basique pour HAproxy pour l'accès à l'API de kubernetes
* Le fichier `deploy_keepalived.yml` permet la mise en place du VRRP entre deux VMs
* Le fichier `deploy_helm.yml` permet de déployer HELM
* Le fichier `deploy_nfs_provisionner.yml` permet de déployer le provisionner NFS
* Le fichier `deploy_dashboard.yml` permet de déployer le Dashboard
* Le fichier `deploy_kong.yml` permet de déployer Kong Ingress Controller
* Le fichier `deploy_all_k8s_components` permet de deployer en nune fois HELM, NFS, DASHBOARD et KONG 

## Architecture cible

*Note* : l'adressage est ici seulement mis à titre d'exemple. Modifiez selon votre infrastructure.  

![Architecture HA K8S](architecture.png)

## Prépartion machine ansible pour le déploiement du cluster HA K8S

### Configuration DNS
Sur la machine qui portera ansible, dans cette exemple nous utiliserons l'une des deux VM Haproxy:
- Installer Ansible avec la [Documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-debian)

Ajouter les entrées DNS dans `/etc/hosts` en indiquant les machines cibles:

Exemple sur la machine configurateur `ansible`:

```bash

# Configurations statiques 

## Loadbalancers
192.168.0.10  lb.domain.com         lb      vip     
192.168.0.11  lb1.domain.com        lb1     vm-ansible
192.168.0.12  lb2.domain.com        lb2  

## Cluster K8S
192.168.0.21  master1.domain.com    master1
192.168.0.22  master2.domain.com    master2
192.168.0.23  master3.domain.com    master3
192.168.0.31  node1.domain.com      node1
192.168.0.32  node2.domain.com      node2
192.168.0.33  node3.domain.com      node3

## Server NFS
192.168.0.40  nfs.domain            nfssrv
```

En plus de cela, ajouter toute les entrées sur votre DNS général pour l'accès au entité suivante:  
- Serveur LB et VIP
- Serveur NFS 
- Machines du cluster Kubernetes
- Serveur Kong (choissiez un Alias sur l'adresse IP de la VIP par exemple)

### Configuration SSH

* Modifier dans `/etc/ssh/sshd_config` le paramètre `PermitRootLogin` à `yes` et 
`StrictModes` à `yes` pour toutes les machines du cluster K8S

* Rebooter chacune de ces machines

* Créer une clé SSH (sans mot de passe) pour le comte `root` de la machine `ansible`

```bash
ssh-keygen -b 4096
```

* Créer un fichier plat avec les nom dns des machines du cluster
```bash 
cat > hosts <<EOL
master1
master2
master3
node1
node2
node3
lb1
lb2
EOL
```
* Deployer la clé SSH root créee sur tout les VM du cluster
  
```bash
cat hosts | xargs -t -I {} ssh-copy-id -i ~/.ssh/id_rsa.pub root@{}
```

* Rebooter chacune de ces machines

### Adapter le fichier inventory

Récupérer le dossier `deployment` de ce projet sur la machine ansible.  
Modifier les adresses ip dans le fichier inventory en fonction de votre architecture.  

```yaml
# Configuration du cluster HA Kubernetes
[firstmaster]
master1 ansible_host=192.168.0.21 ansible_user=root
[othermaster]
master2 ansible_host=192.168.0.22 ansible_user=root
master3 ansible_host=192.168.0.23 ansible_user=root
[workers]
worker1 ansible_host=192.168.0.31 ansible_user=root
worker2 ansible_host=192.168.0.32 ansible_user=root
worker3 ansible_host=192.168.0.33 ansible_user=root
[masters:children]
firstmaster
othermaster
[cluster:children]
masters
workers

# Configuration des machines pour le cluster HaProxy
[lbmaster]
lb1 ansible_host=192.168.0.11 ansible_user=root
[lbslave]
lb2 ansible_host=192.168.0.12 ansible_user=root
[lbs:children]
lbmaster
lbslave

# Configuration de l'interpreter (par défaut)
[all:vars]
ansible_python_interpreter=/usr/bin/python3
```

### Adapter le fichier de variables

Récupérer le dossier `deployment` de ce projet sur la machine ansible.  
Modifier les informations dans le fichier inventory en fonction de votre architecture.  

```yaml
# Docker configuration
docker_ce_version: 5:18.09.0~3-0~ # Don't touch
docker_ce_cli_version: 5:18.09.0~3-0~ # Don't touch

# Kube Configuration
kube_version : 1.19.1-00 # Don't touch
kube_cni : 0.9.7-00 # Don't touch

# Overlay Configuration (Flannel)
pod_network_cidr: 10.244.0.0/16 # Don't touch

# NFS Configuration
NFS_PATH: /mnt/storage/dynamic # Point de montage sur la machine dédiée au NFS
NFS_NETWORK: # Réseau IPV4 ayant droit de se connecter sur le montage NFS
NFS_SERVER: nfs.domain # Nom de domaine du serveur NFS (à configurer en amont)
NFS_CLASS: nfs # Nom de la classe de stockage pour le provisionner de volume Kubernetes

# VRRP/Loadbalancers
loadbalancer_ip: 192.168.0.10 # VIP VRRP
lb1: 192.168.0.11 # Adresse IP VRRP primaire
lb2: 192.168.0.12 # Adresse IP VRRP secondaire
loadbalancer_dns: lb.domain

# Master IP for accessing Kong API on NodePort
MASTER_IP: 192.168.0.21 #First Master, utilisation temporaire pour les besoin de l'installation
KONG_ADMIN_USER: kong_admin # Administrateur 
KONG_API_KEY: 44969152bc4610df7432280f612d33e9 # ApiKey Kong sans tiret !!!!
KONG_ADMIN_DN: kong.domain # Nom de domaine pour l'API Gateway (à configure en amont)

ansible_user: ansible # Nom d'utilisateur ansible
```

### Installation des dépendances

* Installation d'un utilisateur non root (ansible), des différentes dépendences systèmes ainsi que Docker

```bash
ansible-playbook -i inventory install_dependencies.yml
```

### Installation du serveur NFS

* Installation du serveur NFS et création du montage

```bash
ansible-playbook -i inventory deploy_nfs_server.yml
```

### Installation du cluster HAProxy

* Installation du cluster HAProxy et VRRP

```bash
ansible-playbook -i inventory deploy_haproxy.yml
ansible-playbook -i inventory deploy_keepalived.yml
```
### Installation du cluster HA Kubernetes

* Installation de kubernetes, les différentes dépendences ainsi que le réseau Kubernetes (Flannel)

```bash
ansible-playbook -i inventory deploy_ha_cluster.yml
```

### Installer dans les composants externes 

* Installation à faire dans l'ordre: HELM, NFS, Dashboard, Kong Ingress Controller
```bash
ansible-playbook -i inventory deploy_all_k8s_components.yml 
```

### Vérifier le bon fonctionenment du cluster

```bash
ssh ansible@master
```

### Vérifier que le master est up

```bash
kubectl cluster-info
```

### Vérifier que tout les noeuds et les master sont montés avec le statut `ready`

```bash
kubectl get nodes
```

### Vérifier que les composants kubernetes (DNS, Proxy et Flannel) sont en statut `running`

```bash
kubectl get pods -n kube-system
```

### Vérifier le bon fonctionnement du DNS kubernetes

* Deployer dnsutils

```bash
kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
```

* Executer le shell dans le container dnsutils

```bash
kubectl exec -ti dnsutils -* sh
```

* Lancer un nslookup sur le DNS interne

```bash
nslookup kubernetes.default
```

* Lancer nslookup sur un DNS externe

```bash
nslookup www.google.fr
```

* Sortir du shell
`CTRL+D`

### Activer autocompletion sur kubenetes

```bash
echo "source <(kubectl completion bash)" >> ~/.bashrc
```

## Commandes utiles kubernetes

* Se connecter au master depuis la machine ansible

```bash
ssh ansible@master
```

* Afficher les infos du cluster

```bash
kubectl cluster-info
```

* Afficher les nodes du cluster

```bash
kubectl get nodes
```

* Afficher les pods du cluster (par défaut le namespaces est default)

```bash
kubectl get deployments
```

* Afficher les pods du cluster (par défaut le namespaces est default)

```bash
kubectl get pods
```

* Afficher les pods du cluster dans un namespace

```bash
kubectl get pods -n nom_du_namespace
```

* Afficher les pods du cluster dans tout les namespaces

```bash
kubectl get pods --all-namespaces
```

* Afficher les informations concernant un pods

```bash
kubectl describe pod nom_du_pod -n nom_du_namespace
```

* Afficher les logs concernant un pods

```bash
kubectl logs pod nom_du_pod -n nom_du_namespace
```

 Afficher les services du cluster

```bash
kubectl get services
```

* Afficher les services du cluster dans un namespace

```bash
kubectl get services -n nom_du_namespace
```

### Troubleshooting

Suite à un redémarrage du master

* Verifier que swap est désactivé
* Verifier que docker est fonctionnel
* Vérifier que docker est enabled
  Vérifier que kubelet est fonctionnel (service kubelet status)
* export KUBECONFIG=~/.kube/config

* Aide en ligne si besoin : `https://kubernetes.io/docs/setup/production-environment/container-runtimes/`


### Etendre la taille du disque pour des machines non LVM

[Suivre le tutoriel](https://theether.net/kb/100224)
