## Objectif

Ce programme permet de déployer un cluster kubernetes (1 master, 2 workers) à partir d'une machine de contrôle ansible.

## Prérequis

* 4 machines Debian (>=9), 1 pour ansible, 1 pour la master k8s et 2 pour les nodes. (Les hostnames doivent êtres différents!)

| Machine   | Minimum CPU | Minimum RAM |
| ------------- |:-------------:| :-------------:|
| master       | 4       |8 Go|
| noeud      | 2  |4 Go|

* Installer Ansible [Documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-debian)
* Configuration du SSH

## Architecture du programme

Le programme contient plusieurs fichiers/dossier importants:

* Les fichiers `inventory_` Contient l'inventaire des machines distantes
* Le dossier `roles` contient tout les rôles utilisées dans ansible
* Le fichier `prepare.yml`permet d'installer toutes les dépendences nécessaires
* Le fichier `initial.yml`permet de déployer le cluster
* Le fichier `destroy.yml`permet de détruire le cluster

## Installation

### Configuration DNS (hosts)

Sur chaque machines ajouter dans `\etc\hosts` les correspondances @IP-Noms des 4 machines.

Exemple sur la machine `ansible`:

```bash
xxx.xxx.xxx.xxx  yyy.wwww.zz   yyy   master
xxx.xxx.xxx.xxx  yyy.wwww.zz   yyy   worker1
xxx.xxx.xxx.xxx  yyy.wwww.zz   yyy   worker2
xxx.xxx.xxx.xxx  yyy.wwww.zz   yyy   ansible
```

### Configuration SSH

* Modifier dans `/etc/ssh/sshd_config` le paramètre `PermitRootLogin` à `yes` pour les machines suivantes :

```bash
master
worker1
worker2
```

* Rebooter chacune de ces machines

* Créer une clé SSH (sans mot de passe) pour le root de la machine ansible

```bash
ssh-keygen
```

* Ajouter la clé sur les machines distantes (root)

```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub root@master
ssh-copy-id -i ~/.ssh/id_rsa.pub root@worker1
ssh-copy-id -i ~/.ssh/id_rsa.pub root@worker2
```

* Modifier dans `/etc/ssh/sshd_config` le paramètre `PermitRootLogin`, supprimer la ligne pour les machines suivantes :

```bash
master
worker1
worker2
```

* Rebooter chacune de ces machines

### Adapter le fichier inventory

Modifier les adresses ip dans le fichier inventory comme suit.

```yaml
[master]
master ansible_host=XXX.XXX.XXX.XXX ansible_user=root

[workers]
worker1 ansible_host=XXX.XXX.XXX.XXX ansible_user=root
worker2 ansible_host=XXX.XXX.XXX.XXX ansible_user=root

[all:vars]
ansible_python_interpreter=/usr/bin/python3
```

### Installation dépendances

* Installation d'un utilisateur non root (ansible), des différentes dépendences systèmes ainsi que Docker

```bash
ansible-playbook -i inventory prepare.yml
```

### Installation cluster

* Installation de kubernetes, les différentes dépendences ainsi que le réseau kubernetes (Flannel)

```bash
ansible-playbook -i inventory initial.yml
```

### Désinstallation cluster

* :warning: Destruction du cluster

```bash
ansible-playbook -i inventory destroy.yml
```

### Vérifier le bon fonctionnement du cluster

Se connecter sur le master avec le compte ansible:

```bash
ssh ansible@master
```

#### Vérifier que le master est up

```bash
kubectl cluster-info
```

#### Vérifier que tout les noeuds et le master sont montés avec le statut `ready`

```bash
kubectl get nodes
```

#### Vérifier que les composants kubernetes (DNS, Proxy et Flannel) sont en statut `running`

```bash
kubectl get pods -n kube-system
```

#### Vérifier le bon fonctionnement du DNS kubernetes

* Deployer dnsutils

```bash
kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
```

* Executer le shell dans le container dnsutils

```bash
kubectl exec -ti dnsutils -* sh
```

* Lancer un nslookup sur le DNS interne

```bash
nslookup kubernetes.default
```

* Lancer nslookup sur un DNS externe

```bash
nslookup www.google.fr
```

* Sortir du shell
`CTRL+D`

#### Activer autocompletion sur kubenetes

```bash
echo "source <(kubectl completion bash)" >> ~/.bashrc
```

## Commandes utiles kubernetes

* Se connecter au master depuis la machine ansible

```bash
ssh ansible@master
```

* Afficher les infos du cluster

```bash
kubectl cluster-info
```

* Afficher les nodes du cluster

```bash
kubectl get nodes
```

* Afficher les pods du cluster (par défaut le namespaces est default)

```bash
kubectl get deployments
```

* Afficher les pods du cluster (par défaut le namespaces est default)

```bash
kubectl get pods
```

* Afficher les pods du cluster dans un namespace

```bash
kubectl get pods -n nom_du_namespace
```

* Afficher les pods du cluster dans tout les namespaces

```bash
kubectl get pods --all-namespaces
```

* Afficher les informations concernant un pods

```bash
kubectl describe pod nom_du_pod -n nom_du_namespace
```

* Afficher les logs concernant un pods

```bash
kubectl logs pod nom_du_pod -n nom_du_namespace
```

 Afficher les services du cluster

```bash
kubectl get services
```

* Afficher les services du cluster dans un namespace

```bash
kubectl get services -n nom_du_namespac
```

### Troubleshooting

Suite à un redémarrage du master

* Verifier que swap est désactivé
* Verifier que docker est fonctionnel
* Vérifier que docker est enabled
* export KUBECONFIG=~/.kube/config

* Aide en ligne si besoin : `https://kubernetes.io/docs/setup/production-environment/container-runtimes/`
