## Prérequis

- Installer docker (en root)

1. Installer les dépendances

        ```bash
        apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
        ```

2. Ajouter la clé GPG de docker

        ```bash
        curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
        ```

3. Ajouter le repository docker

        ```bash
        add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian \
        $(lsb_release -cs) \
        stable"
        apt update
        ```

4. Installer Docker et autoriser root

        ```bash
        apt -y install docker-ce docker-ce-cli containerd.io
        usermod -aG docker $USER
        newgrp docker
        docker version
        ```

- Installer docker-compose

        ```bash
        curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        docker-compose --version
        ```

## Créer un certificat pour votre domaine

- Créer un dossier ou se trouveront les certificats

        ```bash
        mkdir -p /opt/coffre
        ```

- Récupérer le fichier `generate_certificate.sh` et le déplacer vers le dossier crée en haut

- A l'aide de l'outils `generate_certificate.sh` déployer une certificat pour le nom de domaine de Konga

        ```bash
        cd /opt/coffre
        ./generate_certificate.sh konga.devgalaxy.vp
        ```

## Modifier l'HAProxy

- Dans `/etc/haproxy/haproxy.cfg` ajouter une configuration comme suit:

        ```bash
        frontend front-konga
                bind haproxy.devgalaxy.vp:8443 ssl crt /opt/coffre/konga.devgalaxy.vp/konga.devgalaxy.vp.pem
                acl host_konga hdr(host) -i konga.devgalaxy.vp
                mode http
                option httplog
                use_backend back-konga

        backend back-konga
                mode http
                option forwardfor
                option httpclose
                balance roundrobin
                server konga 127.0.0.1:1337 check ssl verify none
        ```

## Deployer Konga avec docker compose

- Récupérer et modifier le fichier `docker-compose.yml` en modifier la partie `volumes` du service `konga` pour prendre en compte le dossier de certificat créer plus haut

- Lancer le déploiement dans le dossier ou ce trouve le fichier compose

        ```bash
        docker-compose up --detach
        ```
