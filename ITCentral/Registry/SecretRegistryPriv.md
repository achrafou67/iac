## Objectif

Ce guide a pour objectif de créer un secret permettant à K8S de s'authentifier pour reccupper un container dans un registry privé.

## Prérequis

* Avoir un docker registry

## Mise en place

Sur le master k8s.

* Se connecter une première fois sur le registry

```bash
docker login registry.gitlab.com
```

* Récupérer le token d'authentification crée:

```bash
cat ~/.docker/config.json | base64 -w0
```

* Créer un secret `mon-secret.yml` en veillant a changer le namespace en fonction

```yaml
apiVersion: v1
kind: Secret
metadata:
name: gitlab-docker-secret
namespace: NAMESPACE
data:
.dockerconfigjson: LE TOKEN
type: kubernetes.io/dockerconfigjson
```

* Deployer le secret

```bash
kubectl apply -f mon-secret.yml
```

* Utiliser le secret dans un deploiement, rajouter les valeurs yaml suivante sur la même colonne `container`

```yaml
imagePullSecrets:
    - name: gitlab-docker-secret
```
