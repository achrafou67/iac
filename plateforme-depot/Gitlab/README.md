
## Objectif

Ce document permet de déployer notre Gitlab privé

## Prérequis

* 1 NAS avec un partage accessible en NFS
* 1 machine Debian (>=9)

| Machine   | Minimum CPU | Minimum RAM |
| ------------ |:-------------:| :-------------:|
| host-gitlab       | 2vCPU         |8 Go|

## Installation

### Installation de l'OS Debian

[Installation OS Debian](../../procedures/installation_debian/README.md)

#### Mise en place du point de montage du NAS dépôt

[Installation montage NFS](../../procedures/montage_nfs/README.md)

#### Installation Docker

[Installation Docker](../../procedures/installation_docker/README.md)

### Installation Gitlab

* Passer en mode root

```bash
su -
```

#### Récupération les fichiers config

* Créer un répertoire pour le dépot de la config

```bash
mkdir -p /opt/gitlab
```

* Récupérer via ce Gitlab la partie config dans `/opt/gitlab`

#### Configuration du container docker

* Editer le fichier docker-compose.yml

```bash
vi docker-compose.yml
```

Configurer le hostname dans le fichier docker-compose.yml

(Optionnel) : Vous pouvez changer le container_name si vous le souhaitez

`web:
  image: 'gitlab/gitlab-ce:latest'
  restart: always
  container_name : 'scs-gitlab'
  hostname: 'gitlab.devops.vp'
  ports:
      * '80:80'`

#### Configuration de l'application Gitlab

* Editer le fichier gitlab.rb

##### Configuration de l'accés Gitlab

* Configurer le external_url (il doit être égal au hostname)

```yaml
external_url 'https://xxxx.xxxx.xxx'
```

##### Configuration de l'envoi de mail

* Entrer l'email que vous souhaitez pour gitlab

```yaml
# Email de la plateforme GITLAB
gitlab_rails['gitlab_email_from'] = 'xxxxx@cloud-spie.com'
```

* Configurer le serveur ou relai smtp

```yaml
gitlab_rails['smtp_address'] = 'smtp.cloud-spie.com'
```

##### Configuration d'un registry Docker (Optionnel)

Si vous souhaitez activer le mode registry(Docker)

* Positionner redirect_http_to_https à true

* Puis saisisser un FQDN pour le registry (mode HTTPS obligatoire)

```yaml
# Registry config
nginx['redirect_http_to_https'] = true
registry_external_url 'https://yyyy.yyyy.yyy'
```

##### Configuration de l'authentification Active Directory (Optionnel)

Si vous souhaitez activer l'authentification Active Directory

* Positionner ldap_enabled à true

```yaml
gitlab_rails['ldap_enabled'] = true
```

* Ensuite configurer les accés à l'AD (Remplacer __\*\*\*\*TODO\*\*\*\*__ par les bonnes données

```yaml
# Config AD 2016
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
   main: # 'main' is the GitLab 'provider ID' of this LDAP server
       label: '****TODO****'
       host: '****TODO****'
       port: 636                                                       # Change to 636 if using LDAPS
       base: '****TODO****'                                            # dc=XXX,DC=XXX
       uid: 'sAMAccountName'                                           # Don't change this
       bind_dn: '****TODO****'                                         # String bind service user
       password: '****TODO****'
       user_filter: '(memberOf:1.2.840.113556.1.4.1941:=****TODO****)' # CN=XXXX,ou=XXXX,dc=XX,DC=XXX
       encryption: 'simple_tls'
       verify_certificates: false
       timeout: 10
       active_directory: true
       allow_username_or_email_login: false
       block_auto_created_users: false
   EOS
```

###### Configuration des certificats SSL

* Allez dans le répertoire ssl

```bash
cd ssl
```

Déposer ici les fichiers key et crt de vos clés SSL

Ces fichiers doivent être nommés de façon précise.

Pour le external_url dans notre exemple de config nous avons 'xxxx.xxxx.xxx'

Donc dans le répertoire ssl il doit y avoir

`xxxx.xxxx.xx.crt xxxx.xxxx.xx.key`

(des exemples sont fournis, ne pas utiliser en PROD)

Idem si registry docker est activé

Dans notre exemple de config nous avons 'yyyy.yyyy.yyy'

Dans le répertoire ssl il doit y avoir

`yyyy.yyyy.yyy.crt yyyy.yyyy.yyy.key`

(des exemples sont fournis dans le zip, ne pas utiliser en PROD)

##### Configuration du point persistant 'etc'

* Revenir dans le répertoire de 'gitlab.rb'

* Création du répertoire 'etc'

```bash
mkdir -p /mnt/nas-depot/gitlab/etc
```

* Copier les fichiers de conf et ssl

```bash
cp -r * /opt/gitlab/etc
```

### Lancement

* Retourner dans le répertoire du fichier docker-compose.yml

* Lancer le build et l'execution

```bash
docker-compose up -d
```

Le lancement de l'application et son démarrage peut prendre plusieurs minutes.

Pour avoir accès à l'application, lancer un navigateur est tenté l'accés à ce qui a été mis dans 'external_url'

### Configuration Gitlab via l'interface GUI

Allez sur le site via l'url mis dans la variable 'external_url'

* Gitlab va dans un premier temps, demander de changer le mot de passe du compte administrateur (root)

![Image of reset_password](./images/reset_password.png)

* Cliquer sur standard et se logguer en tant que root pour le username et votre nouveau password

(Si l'authetification AD activée, un onglet du label de cette authentification et visible)

![Image of reset_password](./images/connect.png)

* Une fois connecté, se rendre dans le menu :wrench:
* Puis cliquer sur le menu '__Settings > General__'

![Image of settings_admin](./images/settings_admin.png)

* Déplier '__Sign-up restrictions__'

![Image of expand_signup](./images/expand_signup.png)

* Décocher l'option '__Sign-up enabled__'
* Puis cliquer sur le bouton '__Save changes__'

![Image of btn_save](./images/btn_save.png)

* Déplier '__Account and limit__'

![Image of btn_save](./images/limit_projet.png)

* Mettre `0` sur l'option '__Default projects limit__'
* Puis cliquer sur le bouton '__Save changes__'

![Image of btn_save](./images/btn_save.png)

Voilà Gitlab est configuré. Bravo.
