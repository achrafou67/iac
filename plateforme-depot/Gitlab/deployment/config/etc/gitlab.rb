external_url 'https://VOTRE_FQDN'

# Time zone 
gitlab_rails['time_zone'] = 'Europe/Paris'

# Email de la plateforme GITLAB
gitlab_rails['gitlab_email_from'] = '****TODO****'

# Registry config
nginx['redirect_http_to_https'] = false
registry_external_url 'https://VOTRE_FQDN_REGISTRY'

# Config sender mail
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = '****TODO****'
gitlab_rails['smtp_port'] = 25

# Config AD 2016
gitlab_rails['ldap_enabled'] = false
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
   main: # 'main' is the GitLab 'provider ID' of this LDAP server
       label: '****TODO****'
       host: '****TODO****'
       port: 636                                                       # Change to 636 if using LDAPS
       base: '****TODO****'                                            # dc=XXX,DC=XXX
       uid: 'sAMAccountName'                                           # Don't change this
       bind_dn: '****TODO****'                                         # String bind service user
       password: '****TODO****'
       user_filter: '(memberOf:1.2.840.113556.1.4.1941:=****TODO****)' # CN=XXXX,ou=XXXX,dc=XX,DC=XXX 
       encryption: 'simple_tls'
       verify_certificates: false
       timeout: 10
       active_directory: true
       allow_username_or_email_login: false
       block_auto_created_users: false
   EOS

# Config right users
gitlab_rails['gitlab_default_can_create_group'] = false
gitlab_rails['gitlab_username_changing_enabled'] = false

