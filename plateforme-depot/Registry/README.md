## Objectif

Ce programme permet de déployer un Registry Docker privé

## Prérequis

* 1 NAS avec un partage accessible en NFS
* 1 machine Debian (>=10)

| Machine   | Minimum CPU | Minimum RAM |
| ------------ |:-------------:| :-------------:|
| host-registry-docker       | 2 vCPU         | 2 Go |

## Installation

### Installation de l'OS Debian

[Installation OS Debian](../../procedures/installation_debian/README.md)

#### Mise en place du point de montage du NAS dépôt

[Installation montage NFS](../../procedures/montage_nfs/README.md)

#### Installation Docker

[Installation Docker](../../procedures/installation_docker/README.md)

### Installation Registry Docker

* Passer en mode root

```bash
su -
```

#### Récupération les fichiers config

* Créer un répertoire pour le dépot de la config

```bash
mkdir -p /opt/registry
```

* Récupérer via ce Gitlab la partie config dans `/opt/registry`

#### Configuration du FQDN du Registry Docker

* Editer le fichier httpd.conf

```bash
cd /opt/registry
vi conf/httpd.conf
```

#### Configuration du container docker

* Editer le fichier docker-compose.yml

```bash
vi docker-compose.yml
```

Modifier les paramètres AD

* Création du répertoire 'data' et conf

```bash
mkdir -p /mnt/nas-depot/registry/data
mkdir -p /mnt/nas-depot/registry/conf
```

* Copier la config

```bash
cp -r conf/* /mnt/nas-depot/registry/conf
```

#### Configuration du SSL 

* Copier les fichiers crt et key de votre certificat pour Registry Docker

```bash
cp <<CERTIFICATE.CRT>> /mnt/nas-depot/registry/conf/domain.crt
cp <<CERTIFICATE.KEY>> /mnt/nas-depot/registry/conf/domain.key
```

#### Lancement

* Retourner dans le répertoire du fichier docker-compose.yml
* Lancer le build et l'execution

```bash
docker-compose up -d
```

* Tester la connexion

```bash
docker login <FQDN>
```

* S'identifer à la demande

* Refermer la connexion

```bash
docker logout
```

Registry Docker est configuré
