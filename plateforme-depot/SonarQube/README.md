## Objectif

Ce document permet de déployer notre Sonarqube privé

## Prérequis

* 1 NAS avec un partage accessible en NFS
* 1 Serveur PostgreSQL (voir [installation serrveur PostgreSQL](../PostgreSQL/README.md))
* 1 machine Debian (>=9)

| Machine   | Minimum CPU | Minimum RAM |
| ------------* |:-------------:| :-------------:|
| host-sonarqube       | 1vCPU         |4 Go|

## Installation

### Installation de l'OS Debian

[Installation OS Debian](../../procedures/installation_devian/README.md)

#### Mise en place du point de montage du NAS dépôt

[Installation montage NFS](../../procedures/montage_nfs/README.md)

#### Installation Docker

[Installation Docker](../../procedures/installation_docker/README.md)

### Installation de SonarQube

* Passer en mode root

```bash
su -
```

#### Récupération les fichiers config

* Créer un répertoire pour le dépot de la config

```bash
mkdir -p /opt/sonarqube
```

* Récupérer via ce Gitlab la partie config dans `/opt/sonarqube`

#### Configuration du serveur PosgresSQL

* Se connecter en SSH au serveur PostgreSQL

* Se mettre en user postgres

```bash
su * postgres
```

* Basculer en shell postgreSQL

```bash
psql
```

* Créer un user DB pour sonarqube

```bash
CREATE USER sonaruser WITH PASSWORD 'VOTRE_MOT_DE_PASSE';
```

* Créer la DB de sonarqube

```bash
CREATE DATABASE sonarqubedb;
GRANT ALL PRIVILEGES ON DATABASE sonarqubedb to sonaruser;
```

* Sortir du shell et de la machine

```bash
\q
exit
```

* Retourner sur la machine SonarQube

#### Configuration des points persistant

* Création du répertoire 'conf'

```bash
mkdir -p /mnt/nas-depot/sonarqube/conf
```

* Création du répertoire 'plugins'

```bash
mkdir -p /mnt/nas-depot/sonarqube/extensions/plugins
```

* Création du répertoire 'logs'

```bash
mkdir -p /mnt/nas-depot/sonarqube/logs
```

* Création du répertoire 'data'

```bash
mkdir -p /mnt/nas-depot/sonarqube/data
```

* Autoriser les accès à ces répertoires

```bash
chmod -R 777 /mnt/nas-depot/sonarqube/*
```

##### Configuration de Sonarqube

* Editer le fichier docker-compose.yml pour modifier les lignes suivantes

```yaml
    * sonar.jdbc.password=VOTRE_MOT_DE_PASSE
    * sonar.jdbc.url=jdbc:postgresql://(IP ou FQDN serveur postgreSQL):5432/sonarqubedb
    * sonar.core.serverBaseURL=http://VOTRE_FQDN_SONARQUBE
```

Remplacer 'VOTRE_MOT_DE_PASSE', '(IP ou FQDN serveur postgreSQL)' et 'VOTRE_FQDN_SONARQUBE'

##### Déployer les fichiers de Sonarqube

* Déployer les fichiers du répertoire 'conf' dans le répertoire persistant

```bash
cp ./conf/* /mnt/nas-depot/sonarqube/conf/
chmod 666 /mnt/nas-depot/sonarqube/conf/*
```

* Déployer les plugins du répertoire 'plugins' dans le répertoire persitant

```bash
cp ./plugins/* /mnt/nas-depot/sonarqube/extensions/plugins/
chmod 666 /mnt/nas-depot/sonarqube/extensions/plugins/*
```

##### Tunning de l'OS de l'host

* Déployer les fichiers du répertoire 'conf' dans le répertoire persistant

```bash
vi /etc/sysctl.conf
```

* Ajouter cette ligne en fin de fichier

`vm.max_map_count=262144`

* Sauvegarder et quitter fichier

* Rebooter la machine

```bash
reboot
```

* Reconnecter vous à la machine en SSH

### Lancement

* Passer en mode root

```bash
su -
```

* Retourner dans le répertoire du fichier docker-compose.yml

* Lancer le build et l'execution

```bash
docker-compose up -d
```

Le lancement de l'application et son démarrage peut prendre plusieurs minutes.

Pour avoir accès à l'application, lancer un navigateur est tenté l'accés à ce qui a été mis dans __sonar.core.serverBaseURL__

## Configuration SonarQube via l'interface GUI

Allez sur le site via l'url mis dans la variable __sonar.core.serverBaseURL__

* Par défaut sonarqube à un administrateur déjà configuré.

`Login : admin`
`Password : admin`

* Cliquer sur 'Log in' pour indetification

![Image of log_in](./images/log_in.png)

![Image of form_lgpw](./images/form_lgpw.png)

*__`Il faut IMPERATIVEMENT changer le mot de passe`__*

* Cliquer sur en haut à droite sur le __A > My Account > Security__

![Image of change_pwd](./images/change_pwd.png)

### Paramétrage 'General Settings'

* Cliquer sur  __Administration > Configuration > General Settings > General__

![Image of ad_conf_settings](./images/ad_conf_settings.png)

* Configurer les champs concernant 'email'
  * From address : __sonarqube@cloud-spie.com__
  * From name : __SonarQube__
  * SMTP host : __smtp.cloud-spie.com__

![Image of set_emailfrom](./images/set_emailfrom.png)

*Ne pas oublier de valider __CHAQUE__ modification*

![Image of btn_save](./images/btn_save.png)

* Configurer les champs concernant 'base url'
  * Server base URL : __Mettre la même valeur que pour le champ sonar.core.serverBaseURL__

![Image of set_baseurl](./images/set_baseurl.png)

* Cliquer sur Log out pour quitter l'application

![Image of log_out](./images/log_out.png)

Pour une utilisation seul de sonarQube l'installation est terminée.

### Configuration de la liason GitLab <-> SonarQube via l'interface GUI

Ouvrer 2 onglets de votre navigateur préférés

Dans le premier onglet, allez sur le site via l'url mis dans la variable __sonar.core.serverBaseURL__

Dans le deuxième onglet, allez sur le site Gitlab (voir [installation serrveur Gitlab](../Gitlab/README.md))

### Paramétrage dans Gitlab

* S'identifier en tant qu'Administrateur sur Gitlab.

* Une fois connecté, se rendre dans le menu :wrench:
* Puis cliquer sur le menu '__Overview > Users__'

![Image of gitlab_users](./images/gitlab_users.png)

* Cliquer sur le bouton '__New User__'
  * Name : __SonarQube__        `NE SURTOUT PAS CHANGER CE NOM OU l'ORTHOGRAPHE`
  * Username : __SonarQube__        `NE SURTOUT PAS CHANGER CE USERNAME OU l'ORTHOGRAPHE`
  * Email : __scs-devops@spie.com__
  * Can create group : __Non cochée__

![Image of gitlab_new_user](./images/gitlab_new_user.png)

* Cliquer sur '__Create user__'

(Un mail de notification sera envoyé, ignorer le)

* Cliquer sur le menu '__Impersonation Tokens__'

![Image of gitlab_token](./images/gitlab_token.png)

* Saisisser les informations suivantes:
  * Name : __SonarQube__
  * api : __Cochée__
  
![Image of gitlab_new_token](./images/gitlab_new_token.png)

* Cliquer sur '__Create impersonation token__'
  
![Image of gitlab_token_copy](./images/gitlab_token_copy.png)

__`Copier bien le token en quelque part pour l'instant. ATTENTION il ne sera plus donné ensuite donc ne pas oublier.`__

### Paramétrage dans SonarQube

* S'identifier en tant qu'Administrateur sur SonarQube.
* Une fois connecté, se rendre dans le menu '__Administration__'
* Puis cliquer sur le sous menu '__Gitlab__'
  
![Image of menu_gitlab](./images/menu_gitlab.png)

* Saisisser les informations suivantes :
  * GitLab url : __URL DE VOTRE GITLAB INSTALLE__
  * GitLab Ignore Certificate : __OUI__
  * GitLab User Token : __LE FAMEUX TOKEN CREE DANS GITLAB PRECEDEMENT__

![Image of params_gitlab](./images/params_gitlab.png)

*Ne pas oublier de valider __CHAQUE__ modification*

![Image of btn_save](./images/btn_save.png)

* Configurer les formats de retour de SonarQube dans GitLab
* Global template copier :

```bash
<#if qualityGate??>
SonarQube analysis indicates that quality gate is <@s status=qualityGate.status/>.
<#list qualityGate.conditions() as condition>
<@c condition=condition/>

</#list>
</#if>
<#macro c condition>* ${condition.metricName} is <@s status=condition.status/>: Actual value ${condition.actual} is ${condition.symbol}<#if condition.status != OK && condition.message?? && condition.message?trim?has_content> (${condition.message})</#if></#macro>
<#macro s status><#if status == OK>passed<#elseif status == WARN>warning<#elseif status == ERROR>failed<#else>unknown</#if></#macro>
<#assign newIssueCount = issueCount() notReportedIssueCount = issueCount(false)>
<#assign hasInlineIssues = newIssueCount gt notReportedIssueCount extraIssuesTruncated = notReportedIssueCount gt maxGlobalIssues>
<#if newIssueCount == 0>
SonarQube analysis reported no issues.
<#else>
SonarQube analysis reported ${newIssueCount} issue<#if newIssueCount gt 1>s</#if>
    <#assign newIssuesBlocker = issueCount(BLOCKER) newIssuesCritical = issueCount(CRITICAL) newIssuesMajor = issueCount(MAJOR) newIssuesMinor = issueCount(MINOR) newIssuesInfo = issueCount(INFO)>
    <#if newIssuesBlocker gt 0>
* ${emojiSeverity(BLOCKER)} ${newIssuesBlocker} blocker
    </#if>
    <#if newIssuesCritical gt 0>
* ${emojiSeverity(CRITICAL)} ${newIssuesCritical} critical
    </#if>
    <#if newIssuesMajor gt 0>
* ${emojiSeverity(MAJOR)} ${newIssuesMajor} major
    </#if>
    <#if newIssuesMinor gt 0>
* ${emojiSeverity(MINOR)} ${newIssuesMinor} minor
    </#if>
    <#if newIssuesInfo gt 0>
* ${emojiSeverity(INFO)} ${newIssuesInfo} info
    </#if>
    <#if !disableIssuesInline && hasInlineIssues>

Watch the comments in this conversation to review them.
    </#if>
    <#if notReportedIssueCount gt 0>
        <#if !disableIssuesInline>
            <#if hasInlineIssues || extraIssuesTruncated>
                <#if notReportedIssueCount <= maxGlobalIssues>

#### ${notReportedIssueCount} extra issue<#if notReportedIssueCount gt 1>s</#if>
                <#else>

#### Top ${maxGlobalIssues} extra issue<#if maxGlobalIssues gt 1>s</#if>
                </#if>
            </#if>

Note: The following issues were found on lines that were not modified in the commit. Because these issues can't be reported as line comments, they are summarized here:
        <#elseif extraIssuesTruncated>

#### Top ${maxGlobalIssues} issue<#if maxGlobalIssues gt 1>s</#if>
        </#if>

        <#assign reportedIssueCount = 0>
        <#list issues(false) as issue>
            <#if reportedIssueCount < maxGlobalIssues>
1. ${print(issue)}
            </#if>
            <#assign reportedIssueCount++>
        </#list>
        <#if notReportedIssueCount gt maxGlobalIssues>
* ... ${notReportedIssueCount-maxGlobalIssues} more
        </#if>
    </#if>
</#if>
```

* Inline template copier :

```bash
<#list issues() as issue>
<@p issue=issue/>
</#list>
<#macro p issue>
${emojiSeverity(issue.severity)} ${issue.message} [:blue_book:](${issue.ruleLink})
</#macro>
```

![Image of params_comment_gitlab](./images/params_comment_gitlab.png)

*Ne pas oublier de valider __CHAQUE__ modification*

![Image of btn_save](./images/btn_save.png)

Voilà votre SonarQube est configuré. Bravo.
