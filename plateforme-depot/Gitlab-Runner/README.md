
## Objectif

Ce programme permet de déployer un Gitlab Runner privé

## Prérequis

* 1 NAS avec un partage accessible en NFS
* 1 machine Debian (>=9)
* 1 machine Gitlab installé ([Installation Gitlab](../Gitlab/README.md))

| Machine   | Minimum CPU | Minimum RAM |
| ------------ |:-------------:| :-------------:|
| host-gitlab-runner       | 2vCPU         |4 Go|

## Installation

### Installation de l'OS Debian

[Installation OS Debian](../../procedures/installation_debian/README.md)

#### Mise en place du point de montage du NAS dépôt

[Installation montage NFS](../../procedures/montage_nfs/README.md)

#### Installation Docker

[Installation Docker](../../procedures/installation_docker/README.md)

### Installation Gitlab Runner

* Passer en mode root

```bash
su -
```

#### Récupération les fichiers config

* Créer un répertoire pour le dépot de la config

```bash
mkdir -p /opt/gitlab-runners
```

* Récupérer via ce Gitlab la partie config dans `/opt/gitlab-runners`

#### Configuration du container docker

* (Optionnel) Editer le fichier docker-compose.yml

```bash
vi docker-compose.yml
```

(Optionnel) : Vous pouvez changer le container_name si vous le souhaitez

`web:
gitlab-runner:
  image: 'gitlab/gitlab-runner'
  restart: always
  container_name: 'scs-gitlab-runner'`

#### Configuration du point persistant 'etc'

* Création du répertoire 'etc'

```bash
mkdir -p /mnt/nas-depot/gitlab-runner/etc
```

* Copier la config

```bash
sudo cp -r config/* /mnt/nas-depot/gitlab-runner/etc
```

*__`Chapitre à suivre UNIQUEMENT pour des certificats SSL auto-signé pour le site Gitlab`__*

#### Configuration du SSL auto-signé du Gitlab

* Copier les fichiers crt et key de votre certificat Gitlab

```bash
cp <<CERTIFICATE.CRT>> /mnt/nas-depot/gitlab-runner/etc/certs/FQDN_SITE_GITLAB.crt
cp <<CERTIFICATE.KEY>> /mnt/nas-depot/gitlab-runner/etc/certs/FQDN_SITE_GITLAB.key
cp <<CERTIFICATE.crt>> /usr/local/share/ca-certificates/FQDN_SITE_GITLAB.crt
update-ca-certificates
```

#### Lancement

* Retourner dans le répertoire du fichier docker-compose.yml
* Lancer le build et l'execution

```bash
docker-compose up -d
```

* Une fois le message suivant apparu et le prompt rendu

`Status: Downloaded newer image for gitlab/gitlab-runner:latest
Creating scs-gitlab-runner ... done`

Gitlab Runner est lancé mais pas encore lié à Gitlab.

* Afficher les logs afin de suivre le process

```bash
docker logs -f scs-gitlab-runner
```

* Là des logs doivent aparaitre et ressembler à cela

`Runtime platform                                    arch=amd64 os=linux pid=6 revision=577f813d version=12.5.0
Starting multi-runner from /etc/gitlab-runner/config.toml ...  builds=0
Running in system-mode.

Configuration loaded                                builds=0
Locking configuration file                          builds=0 file=/etc/gitlab-runner/config.toml pid=6
listen_address not defined, metrics & debug endpoints disabled  builds=0
[session_server].listen_address not defined, session endpoints disabled  builds=0`

### Configuration du runner Gitlab

* Se rendre sur l'interface de Gitlab
* S'identifier en tant qu'Administrateur sur Gitlab.
* Une fois connecté, se rendre dans le menu :wrench:
* Puis cliquer sur le menu '__Overview > Runners__'

![Image of gitlab_url_runners](./images/gitlab_menu_runners.png)

* Ouvrir une nouvelle session SSH sur la machine host-gitlab-runner

* Lancer le configurateur du Gitlab Runner docker

```bash
docker exec -it scs-gitlab-runner gitlab-runner register
```

* Là un prompt doit aparaitre et ressembler à cela

```bash
Runtime platform                                    arch=amd64 os=linux pid=13 revision=577f813d version=12.5.0
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
```

* En dessus de l'invite

`Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):`

* Copier l'url du Gitlab qui se trouve dans l'interface de '__Overview > Runners__'

![Image of gitlab_url_runners](./images/gitlab_url_runners.png)

* Valider une fois copié

* L'invite suivant apparait

`Please enter the gitlab-ci token for this runner:`

* Copier le toker du Gitlab qui se trouve dans l'interface de '__Overview > Runners__'

![Image of gitlab_token_runners](./images/gitlab_token_runners.png)

* Valider une fois copié

* L'invite suivant apparait

`Please enter the gitlab-ci description for this runner:`

* Mettre une description/nom a ce runner : __Test Code runner__
* Valider une fois copié
* L'invite suivant apparait

`Please enter the gitlab-ci tags for this runner (comma separated):`

* Valider sans rien mettre, on verra plutard dans la GUI de Gitlab
* L'invite suivant apparait

`Registering runner... succeeded                     runner=gHLsPCz6
Please enter the executor: ssh, docker-ssh+machine, shell, virtualbox, docker+machine, kubernetes, custom, docker, docker-ssh, parallels:`

* Saisir : __docker__
* Valider

* L'invite suivant apparait

`Please enter the default Docker image (e.g. ruby:2.6):`

* Saisir : __alpine:latest__
* Valider

* Le message suivant apparait

`Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!`

* Aller dans l'interface de Gitlab
* Rafraichir la page

![Image of gitlab_run_runners](./images/gitlab_run_runners.png)

Gitlab-runner est configuré
