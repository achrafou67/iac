
## Objectif

Ce document permet de déployer notre serveur PostgreSQL

## Prérequis

* 1 NAS avec un partage accessible en NFS
* 1 machine Debian (>=9)

| Machine   | Minimum CPU | Minimum RAM |
| ------------ |:-------------:| :-------------:|
| host-db       | 1vCPU         |1 Go|

## Installation

### Installation de l'OS Debian

[Installation OS Debian](../../procedures/installation_debian/README.md)

#### Mise en place du point de montage du NAS dépôt

[Installation montage NFS](../../procedures/montage_nfs/README.md)

### Installation de PostgreSQL

* Passer en root

```bash
su -
```

* Mettre à jour le cache apt

```bash
apt update
```

* Installer le paquet 'gnupg2'

```bash
apt install gnupg2 rsync
```

* Installer le dépôt

```bash
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
```

* Installer la clé et l'emplacement du dépôt Docker

```bash
wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O * | apt-key add -
apt update
```

* Installer PostgreSQL

```bash
apt-get install postgresql postgresql-contrib
```

* Contrôler PostgreSQL

```bash
systemctl status postgresql
```

* Stopper le serveur PostgreSQL

```bash
systemctl stop postgresql
```

* Contrôler que PostgreSQL est bien arrètè

```bash
systemctl status postgresql
```

* Création de la nouvelle zone de DB sur dépot

```bash
cd /mnt/nas-depot
mkdir db-depot
cd db-depot
mkdir -p var/lib
chmod -R 755 var
```

* Transférer les datas de PostgreSQL

```bash
rsync -av /var/lib/postgresql /mnt/nas-depot/db-depot/var/lib/
```

* Renommer l'ancien chemin

```bash
mv /var/lib/postgresql /var/lib/postgresql.back
```

* Editer le fichier de configuration de PostgreSQL (/12/ car c'est une version 12 dans l'exemple)

```bash
vi /etc/postgresql/12/main/postgresql.conf
```

* Modifier la ligne (/12/ car c'est une version 12 dans l'exemple)

`#data_directory = '/var/lib/postgresql/12/main'         # use data in another directory`

* En (/12/ car c'est une version 12 dans l'exemple)

`data_directory = '/mnt/nas-depot/db-depot/var/lib/postgresql/12/main'         # use data in another directory`

* Autoriser l'écoute de toutes les interfaces par la DB

* Modifier la ligne

`#listen_addresses = 'localhost'          # what IP address(es) to listen on;`

* En

`listen_addresses = '*'          # what IP address(es) to listen on;`

* Quitter en sauvegardant.

* Autoriser les connexions du réseau dépôt à la DB (/12/ car c'est une version 12 dans l'exemple)

```bash
vi /etc/postgresql/12/main/pg_hba.conf
```

* Ajouter la ligne

`host    all             all             192.168.122.0/24            md5`

* Redémarrer le service postgresql

```bash
systemctl restart postgresql
```

* Changer le mot de passe du user postgres

```bash
passwd postgres
```

* Quitter la session

```bash
exit
exit
```

Voilà votre PostgreSQL est configuré. Bravo.
