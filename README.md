
## Objectif

Ce guide vous permet de retrouver l'ensemble des procédures d'installation des applications, outils utilisées pour le développement.
Il contient aussi un ensemble de process métiers à appliquer.

Les documentations sont rangées en fonction de thèmes.

## Thème : Installation Plateforme DEPOT

- [Installation PostgreSQL](./plateforme-depot/PostgreSQL/README.md)
- [Installation Gitlab](./plateforme-depot/Gitlab/README.md)
- [Installation SonarQube](./plateforme-depot/SonarQube/README.md)
- [Installation Gitlab Runner](./plateforme-depot/Gitlab-Runner/README.md)
- [Installation Registry Docker](./plateforme-depot/Registry/README.md)

## Thème : Procédures intallations systémes

- [Installation OS Debian](./procedures/installation_debian/README.md)
- [Installation Docker](./procedures/installation_docker/README.md)
- [Installation montage NFS](./procedures/montage_nfs/README.md)

## Thème : Intallation Plateforme Kubernetes

Ce guide a pour objectif de donner les bases pour l'installation et la mise en place des composants essentiels pour une archiecture Microservices sous K8S.

Les différentes étapes à réaliser sont décrites dans l'ordre dans le sommaire suivant.  

1. [Déploiement d'un cluster K8S haute disponibilité avec ansible ainsi que touts les composants du socle](./ITCentral/Socle-K8S/Cluster-HA-Kubernetes-Kong-Helm-NFS/README.md)

2. [Déploiement d'un cluster K8S avec ansible](./ITCentral/Socle-K8S/Cluster-simple-Kubernetes/README.md)
3. [Mise en place d'un provisionner de volume](./ITCentral/Socle-K8S/Serveur-NFS-and-Provisionner/README.md)
4. [Mise en place de Ingress Contoller Kong, Kong Proxy et HAproxy](./ITCentral/Socle-K8S/Kong-Ingress-Controller/README.md)
5. [Tutorial sur la génération de certificat k8s et utilisation dans un ingress](./ITCentral/Socle-K8S/kubernetes/README.md)
6. [Mise en place de Konga](./ITCentral/Socle-K8S/Konga/README.md)
7. [Mise en place de l'API GW Kong et raccordement à Konga](./ITCentral/Socle-K8S/Kong-API-Gateway/README.md)
8. [Mise en place d'un endpoint d'authentification LDAP raccordé à Kong](./ITCentral/Socle-K8S/Kong-ldap/README.md)
9. [Mise en place de RabbitMQ avec raccordement LDAP](./ITCentral/Socle-K8S/Simple-RabbitMQ/README.md)
10. [Mise en place d'un secret pour un registry privé](./ITCentral/Registry/SecretRegistryPriv.md)
